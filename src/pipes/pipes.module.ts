import { NgModule } from '@angular/core';
import { TimeAgoPipe } from './time-ago/time-ago';
import { FirebaseErrorsPipe } from './firebase-errors/firebase-errors';
@NgModule({
	declarations: [TimeAgoPipe,
    FirebaseErrorsPipe],
	imports: [],
	exports: [TimeAgoPipe,
    FirebaseErrorsPipe]
})
export class PipesModule {}
