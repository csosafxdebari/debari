import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, Events } from 'ionic-angular';

import { LocalDatabaseProvider } from '../../providers/local-database/local-database';
import { AwaitProvider } from '../../providers/await/await';
import { AuthProvider } from '../../providers/auth/auth';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-private-message',
  templateUrl: 'private-message.html',
})
export class PrivateMessagePage {
  @ViewChild("pageContent") content: Content;

  private loading: boolean = true;
  private roomId: string;
  private uid: string;
  private buddyId: string;
  private buddyName: string;
  private message: string;
  private messages = [];
  private unReadMessagesListener;
  private scroll: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private _LOCAL_DATABASE: LocalDatabaseProvider,
    private _AWAIT: AwaitProvider,
    private _AUTH: AuthProvider,
    private _DB: DatabaseProvider,
    private event: Events) {
    this.uid = this._AUTH.getUid();
    this.roomId = this.navParams.get('roomId'); // falta cuando se ingresa desde el perfil
    this.buddyId = this.navParams.get('buddyId');
    this.buddyName = this.navParams.get('buddyName');


  }

  ionViewDidEnter() {
    console.log("escuchando mensajes");
    this.unReadMessagesListener = this.getUnreadMessages();
  }

  callFunction(){
    if(this.scroll) {
      this.content.scrollToBottom(0);
      this.scroll = false;
    }
  }

  ionViewDidLoad() {

    this.getMessages()
    .then((result)=>{
      this.loading = false;
      this.messages = result;
    })
    .catch((err)=> {
      this.loading = false;
    });

    this.unReadMessagesListener = this.getUnreadMessages();

    this._LOCAL_DATABASE.markMessagesAsViewed(this.roomId)
    .then(()=> {
      this.event.publish("room:messagesViewed", this.roomId);
    })
    .catch(err => {
      console.log(err);
    });
  }

  ionViewWillLeave() {
    console.log("dejando de escuchar mensajes");
    this.unReadMessagesListener.unsubscribe();
  }

  async getMessages() {
    let err, result;

    [err, result] = await this._AWAIT.to(this._LOCAL_DATABASE.getMessages(this.roomId));
    if(err) return Promise.reject(err);

    console.log("mensajes bd");
    console.log(result);
    console.log(this.content.ionScroll);

    return Promise.resolve(result);
  }

  async getUnreadMessages() {
    // corregir cuando aun no hay un roomId
    return this._DB.getUnreadMessages(this.roomId).onSnapshot(msgResult => {
      msgResult.docs.forEach(msg => {
        let messageId = msg.id;
        let message = msg.data().message;
        let date = msg.data().date;
        let senderId = msg.data().senderId;
        this.addMessage(this.roomId, {msgId: msg.id, message: message, date: date, senderId: senderId} );
      });
    });
  }

  async addMessage(roomId, data) {
    let err, result;

    console.log("roomId: " + roomId);
    
    [err, result] = await this._AWAIT.to(this._LOCAL_DATABASE.insertPrivateMessage(roomId, data));
    if(err) return Promise.reject(err);

    [err, result] = await this._AWAIT.to(this._DB.markRoomAsSaved(this.roomId, data.msgId));
    if(err) return Promise.reject(err);

    this.messages.push({id: data.msgId, date: data.date, message: data.message, senderId: data.senderId, sent: data.sent, viewed: data.viewed});

    console.log("this.messages");
    console.log(this.messages);

    return Promise.resolve(true);
  }

  async sendMessage(roomId) {
    let err, result;
    let msgObj = {message: this.message, senderId: this.uid};

    const roomObj = {
      buddyId: this.buddyId,
      buddyName: this.buddyName,
      message: this.message
    };
  
    this.message = "";

    msgObj["date"] = new Date();

    this.messages.push(msgObj);
    this.content.scrollToBottom();
  
    if(this.roomId == null) {
      [err, result] = await this._AWAIT.to(this._DB.createChatRoom(this.buddyId));
      this.roomId = result;
      this.unReadMessagesListener = this.getUnreadMessages();
    }

    roomObj["id"] = this.roomId;

    [err, result] =  await this._AWAIT.to(this._LOCAL_DATABASE.insertChatRoom(roomObj));
    if(err) console.log(err);
  
    [err, result] = await this._AWAIT.to(this._DB.sendPrivateMessage(msgObj, this.roomId, this.buddyId));
    if(err) console.log(err); //ver luego como manejar por debajo nomas los errores

    msgObj["msgId"] = result;
    msgObj["date"] = new Date();

    [err, result] = await this._AWAIT.to(this._LOCAL_DATABASE.insertPrivateMessage(this.roomId, msgObj));
    if(err) console.log(err);

    const id: string = this.roomId;
    const buddyName = this.buddyName;
    const message: string = msgObj.message;
    const date: string = msgObj["date"];

    [err, result] =  await this._AWAIT.to(this._LOCAL_DATABASE.updateChatRoom({id, buddyName, message, date}));
    if(err) console.log(err);

    this.event.publish('room:updated', this.roomId, message);

    return Promise.resolve(true);
  }
}
