import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  IonicPage,
  NavController,
  NavParams,
  ActionSheetController,
  MenuController,
  Events,
  PopoverController,
  Slides,
  ModalController,
  AlertController } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { VideoEditor } from '@ionic-native/video-editor';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

import { DatabaseProvider } from '../../providers/database/database';
import { ImageProvider } from '../../providers/image/image';
import { PreloaderProvider } from '../../providers/preloader/preloader';
import { StorageProvider } from '../../providers/storage/storage';
import { AuthProvider } from '../../providers/auth/auth';
import { LocalDatabaseProvider } from '../../providers/local-database/local-database';
import { AlertProvider } from '../../providers/alert/alert';
import { GeocodingProvider } from './../../providers/geocoding/geocoding';
import { GeolocationProvider } from './../../providers/geolocation/geolocation';

import moment from 'moment';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import { DiagnosticProvider } from '../../providers/diagnostic/diagnostic';

declare var google;

@IonicPage()
@Component({
  selector: 'page-manage-alert',
  templateUrl: 'manage-alert.html',
})
export class ManageAlertPage {
  @ViewChild('slides') slides: Slides;
  @ViewChild('map') mapElement: ElementRef;
  alertForm: any;
  isEditable: boolean = false;
  title: string = 'Cargar Alerta';
  alertDate: string;
  alertHour: string;
  chapasNo: Array<any> = [];
  description: string;
  alertType: number = 0;
  images: Array<any> = [];
  imgSlides: Array<any> = [];
  imgsToUpload: Array<any> = [];
  imgsToDelete: Array<any> = [];
  alert: any = [];
  alreadySavedImages: Array<any> = [];
  photoURL: string;
  lat: number;
  lng: number;
  map: any;
  markers: any = [];
  showMap: boolean = false;
  showAlertMessage: boolean = false;
  location: string = 'Ninguna seleccionada';
  loadingFile: boolean = false;
  alertCity: string;
  alertCountry: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private _FB: FormBuilder,
    private _DB: DatabaseProvider,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    private _IMG: ImageProvider,
    private _LOADER: PreloaderProvider,
    private _STORAGE: StorageProvider,
    public authData: AuthProvider,
    public _ALERT: AlertProvider,
    public menu: MenuController,
    public events: Events,
    public _LOCALDB: LocalDatabaseProvider,
    private popoverCtrl: PopoverController,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private videoEditor: VideoEditor,
    private geocoding: GeocodingProvider,
    private geolocation: GeolocationProvider,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    private _LOCAL_STORAGE: LocalStorageProvider,
    private _DIAGNOSTIC: DiagnosticProvider) {

      events.subscribe('file:loading', () => {
        this.loadingFile = true;
      });

      events.subscribe('file:loaded', () => {
        this.loadingFile = false;
      });

	    this.menu.swipeEnable(false);
      events.subscribe('date:change', (date) => {
        this.alertDate = date;
      });

    this.events.subscribe('location:selected', result => {	  
      this.showAlertMessage = true;
      this.lat = result.lat;
	  
      this.lng = result.lng;
      this.location = this.lat + ',' + this.lng;
      this.showMap = true;

      this.geocoding.reverseGeocode(this.lat, this.lng)
      .then((result)=>{
        console.log(result);

        let found = result.find((geo)=>{
          return geo.subAdministrativeArea;
        });

        if(found) {
          this.alertCity = found.subAdministrativeArea;
        }
        else {
          let locality = result.find((geo)=>{
            return geo.locality;
          });

          if(locality) {
            this.alertCity = locality.locality;
          }
          else {
            this.alertCity = result[0].administrativeArea;
          }
        }

        this.alertCountry = result[0].countryCode;

      })
      .catch((err)=>{
        console.log(err);
      });
  
      this.loadMap();
    });

    let currentDate: string = moment().format('DD/MM/YYYY'),
      currentHour: string = moment().format('HH:mm');

    this.alertDate = currentDate;
    this.alertHour = currentHour;

    this.alertForm = _FB.group({
      alertDate: [Validators.compose([Validators.required])],
      alertHour: [Validators.compose([Validators.required])],
      alertType: [this.alertType],
      chapasNo: [this.chapasNo],
      description: ['', Validators.compose([Validators.minLength(5), Validators.required])],
      images: [],
      location: [this.location]
    });

    this.alert = this.navParams.get('alert');

    if (this.alert != null) {
          this.alertDate = this.alert.alertDate.substring(0, 10);
          this.alertHour = this.alert.alertDate.substring(this.alert.alertDate.length - 5);
          this.chapasNo = this.alert.chapasNo;
          this.description = this.alert.description;
          this.imgSlides = this.alert.images;
          this.images = this.alert.images;
         /* this.imgSlides.forEach(img => {
          this.images.push(img.src);
          });*/

          if (this.alert.location != undefined) {
            this.lat = this.alert.location.lat;
            this.lng = this.alert.location.lng;
            this.location = this.lat + ',' + this.lng;
          }

          if(this.alert.uid == this.authData.getUid()) {
            this.isEditable = true;
            this.title = 'Editar Alerta';
          }
          else {
            this.title = 'Alerta';
            this.alertForm.disable();
          }

    }
  }

  ionViewDidEnter() {
    this.alertForm.valueChanges.subscribe(val => {
        this.showAlertMessage = true;
    });
  }

  ionViewWillEnter() {
    if(this.lat != null) {
      this.showMap = true;
      this.loadMap();
    }
  }

  ionViewCanLeave() {
    if (this.showAlertMessage) {
      let alertConfirmExit = this.alertCtrl.create({
        title: 'Confirmación',
        message: '¿Estás seguro que deseas salir? Se perderán todos los cambios.',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('cancel exit');
            }
          },
          {
            text: 'Ok',
            handler: () => {
              this.closePage();
            }
          }
        ]
      });

      alertConfirmExit.present();

      return false;
    }
  }

  saveAlert(): void {

    this._LOADER.displayPreloader();

    let alertData = {
      alertForm: this.alertForm.value,
      images: this.images
    };
    
    if (this.lat != null) {
          let location = {};
          location["lat"] = this.lat;
          location["lng"] = this.lng;
          alertData["location"] = location;
          alertData["alertCity"] = this.alertCity;
          alertData["alertCountry"] = this.alertCountry;
        }

    alertData["imgSlides"] = this.imgSlides;

    if (this.isEditable) {
      alertData["registDate"] = this.alert.registDate;

      this._DB.updateAlertFase1(this.alert.id, alertData, this.imgSlides);
      this._LOADER.hidePreloader();
      this.showAlertMessage = false;
      this.navCtrl.pop();
    }
    else {

      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
        result => {
          if(!result.hasPermission) {
            this._DB.saveAlert(alertData, this.imgSlides);
            this._LOADER.hidePreloader();
            this.showAlertMessage = false;
            this.navCtrl.pop();
          }
          else {
            this._DIAGNOSTIC.isGpsLocationEnabled()
            .then((result) => {

              if(result) {
                this.geolocation.getCurrentPosition()
                .then((result)=>{
          
                  alertData["userLocation"] = { lat: result.coords.latitude, lng: result.coords.longitude };
          
                  this._DB.saveAlert(alertData, this.imgSlides);
                  this._LOADER.hidePreloader();
                  this.showAlertMessage = false;
                  this.navCtrl.pop();
                })
                .catch((err)=>{
                  console.log(err);
                });
              }
              else {
                this._DB.saveAlert(alertData, this.imgSlides);
                this._LOADER.hidePreloader();
                this.showAlertMessage = false;
                this.navCtrl.pop(); 
              }
            });
          }
        });
    }
  }

  updateDocument() {
    let chapasNo: Array<any> = this.alertForm.controls["chapasNo"].value;
    let obj = {};

    chapasNo.forEach((doc) => {
      obj[doc] = true;
    });

    let 
      alertHour: string = this.alertForm.controls["alertHour"].value,
      description: string = this.alertForm.controls["description"].value,
      images: any = this.images,
      registDate: string = moment().format(),
      alertDate: string;

    alertDate = this.alertForm.controls["alertDate"].value + " " + alertHour;

    let alert: any = {
      updateDate: registDate,
      alertDate: alertDate,
      chapasNo: obj,
      description: description,
      images: images
    };

    if (this.lat != null) {
      let location = {};
      location["lat"] = this.lat;
      location["lng"] = this.lng;

      alert["location"] = location;
    }

    //this._DB.updateAlert(alert);
    this._LOADER.hidePreloader();
    this.showAlertMessage = false;
    this.navCtrl.pop();
  }

  takePicture(sourceType, mediaType) {

    let repeated: boolean = false;

	  if(this.imgSlides.length <= 10) {
      

		  this._IMG.takePicture(sourceType, mediaType)
		  .then((imagePath) => {

        if(imagePath != true) {

          this.imgSlides.forEach(img => {
            let pos1 = img.src.indexOf('?');
            let pos2 = imagePath.src.indexOf('?');

            if (pos1 == -1) {
              pos1 = img.src.length;
            }

            if (pos2 == -1) {
              pos2 = imagePath.src.length;
            }

            let img1 = img.src.substring(0, pos1);
            let img2 = imagePath.src.substring(0, pos2);

            if (img1 == img2) {
              repeated = true;
            }

          });
          
          imagePath["mediaType"] = mediaType;

          if (!repeated) {
            if (mediaType == 1) {
              let thumbnailOptions = {
                fileUri: imagePath.src,
                outputFileName: imagePath.name
              };

              this.videoEditor.createThumbnail(thumbnailOptions)
                .then(result => {
                  imagePath["thumbnail"] = 'file://' + result;

                  this.imgSlides.push(imagePath);
                  this.imgsToUpload.push(imagePath);
                  let index = this.imgsToUpload.length;
                  if(index > 1) {
                    this.slides.slideTo(index, 1, false);
                  }
                  this.showAlertMessage = true;
                })
                .catch(err => { console.log(err) });
            }
            else {
              
              this.imgSlides.push(imagePath);
              this.imgsToUpload.push(imagePath);
              let index = this.imgsToUpload.length;
              if(index > 1) {
                this.slides.slideTo(index, 1, false);
              }
              
              this.showAlertMessage = true;

            }
          }

        }
        else {
          console.log('no image selected');
        }
      })
      .catch((err) => {
          this.loadingFile = false;
          this._ALERT.presentAlert('Error', err);
      });

	  }
	  else
	  {
      this._ALERT.presentAlert('Selección', 'Sólo se puede seleccionar hasta 10 archivos');
    }
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Origen',
      buttons: [
        {
          text: 'Imágenes',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, this.camera.MediaType.PICTURE);
          }
        },
        {
          text: 'Vídeos',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, this.camera.MediaType.VIDEO);
          }
        },
        {
          text: 'Cámara',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA, this.camera.MediaType.ALLMEDIA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  deleteFile(i, event) {
    event.stopPropagation();
    let src: string = this.imgSlides[i].src.toString();
    let index = src.indexOf("https");

    if (i == this.imgSlides.length - 1) {
      this.slides.slideTo(this.imgSlides.length - 1, 1, false);
    }

    if (index.toString() == "0")
    {
      this.imgSlides[i]["delete"] = 1;
      this.imgsToDelete.push(this.imgSlides[i]);
    } 
    else {
      this.imgSlides.splice(i, 1);
      this.imgsToUpload.splice(i, 1);
    }

  }

  getFileType(file: string): string {
    let fileName = file.substring(file.lastIndexOf('/') + 13, file.lastIndexOf('?'));
    let type = fileName.substr(fileName.lastIndexOf('.') + 1);

    return type;
  }
  
  onChange(val) {
    let chapa = this.chapasNo[this.chapasNo.length - 1].substr(0, 10).toUpperCase();
    if (chapa != null) {
      this.chapasNo[this.chapasNo.length - 1] = chapa;
    }
  }

  onBlur(val) {
    let chapa = val.trim().toUpperCase().substr(0, 10);

    if (chapa != '') {
      this.chapasNo.push(chapa);
    }
  }
  
  presentPopover() {

    let popover = this.popoverCtrl.create('CalendarPage', {date: this.alertDate}, { cssClass: 'calendar-popover' });

    popover.present();
  }

  deactivate(event){
    
    event.preventDefault();

  }

  presentFullImageModal(images: any, i: number) {
    let fullImageModal = this.modalCtrl.create('FullImagePage', { images: images, index: i });
    fullImageModal.present();
  }

  openPage(component) {
    this.showAlertMessage = false;
/*
    if(this.showMap){
      this.mapPrev.remove();
    }
*/
    this.showMap = false;
    this.navCtrl.push(component, { lat: this.lat, lng: this.lng });
  }

  loadMap() {
    console.log('loadmap');
    let latLng = new google.maps.LatLng(this.lat, this.lng);

    let mapOptions = {
	  zoomControl: false,
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
	  scaleControl: false,
      draggable: false,
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.placeMarker(latLng);
  }

  placeMarker(location) {
    this.markers.forEach(marker => {
      marker.setMap(null);
    });

    this.markers = [];

    let marker = new google.maps.Marker({
      position: location,
      map: this.map
    });

    this.markers.push(marker);
  }

  private closePage() {
    this.showAlertMessage = false;
    this.navCtrl.pop();
  }
}
