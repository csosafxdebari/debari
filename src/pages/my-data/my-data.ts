import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, CheckboxRequiredValidator } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-my-data',
  templateUrl: 'my-data.html',
})
export class MyDataPage {
  public dataForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {

    this.dataForm = formBuilder.group({
      ci: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(8), Validators.required])],
      birthday: ['', Validators.compose([Validators.required])],
      age: [''],
      phoneNumber: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(6), Validators.required])],
      direction: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(50), Validators.required])]
      //,dirReference: ['', Validators.compose([Validators.minLength(10), Validators.requiredTrue])]
    });
  }

  ionViewDidLoad() {

  }

}
