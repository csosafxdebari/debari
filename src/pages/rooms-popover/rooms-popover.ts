import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  Events,
  ViewController } from 'ionic-angular';

import { AlertProvider } from '../../providers/alert/alert';
import { AlertController } from 'ionic-angular';
import { LocalDatabaseProvider } from '../../providers/local-database/local-database';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-rooms-popover',
  templateUrl: 'rooms-popover.html',
})
export class RoomsPopoverPage {
  public options: any = [];

  constructor(
    public viewCtrl: ViewController,
    private navParams: NavParams,
    private navCtrl: NavController,
    private _ALERT: AlertProvider,
    private alertCtrl: AlertController,
    private _LOCAL_DB: LocalDatabaseProvider,
    public toastCtrl: ToastController,
    public events: Events
  ) {

  }

  ngOnInit() {
    if (this.navParams.data) {
      this.options = this.navParams.data.options;
    }
  }

  makeOption(option) {
    if (option.optionName != "Eliminar") {
      this.navCtrl.push(option.component, { room: option.room });
    }
    else {
      this.deleteChatRoom(option.room.id);
    }

    this.close();
  }

  deleteChatRoom(roomId) {
    let alert = this.alertCtrl.create({
      title: 'Confirmación',
      message: '¿Está seguro que desea eliminar este chat?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sí',
          handler: () => {
            this._LOCAL_DB.deleteChatRoom(roomId)
              .then(result => {
                this.presentToast('Chat eliminado');
                this.events.publish('room:deleted', roomId);
                this.navCtrl.pop();
              })
              .catch(err => {
                this._ALERT.presentAlert('Error', err.message);
              });
          }
        }
      ]
    });
    alert.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
