import { Component, ViewChild } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  Searchbar,
  AlertController,
  ToastController} from 'ionic-angular';

import { LocalDatabaseProvider } from '../../providers/local-database/local-database';
import { DatabaseProvider } from '../../providers/database/database';
import { TruncateCharactersPipe } from '@yellowspot/ng-truncate/dist/truncate-characters.pipe';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  @ViewChild('mySearchbar') searchbar: Searchbar;
  searchHistory: any = [];
  alerts: any = [];
  searched: boolean = false;
  offset: string = null;
  uid: string;
  public readLimit: number = 90;
  option: string = 'chapas';
  users: any = [];
  public firstAlert: string;
  public firstUser: string;
  private loading: boolean = false;
  private noResultsFound: boolean = false;
  private mode: string;
  private placeholderTitle: string = 'buscar';

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private _LOCAL_DATABASE: LocalDatabaseProvider,
    private _DB: DatabaseProvider,
    private popoverCtrl: PopoverController,
    private alertCtrl: AlertController,
    public toastCtrl: ToastController) {

      this.mode = this.navParams.get('mode');

      if(this.mode == 'receivedAlerts') {
        this.placeholderTitle = 'Buscar en mis alertas recibidas';
      }
      else if(this.mode == 'createdAlerts') {
        this.placeholderTitle = 'Buscar en mis alertas creadas';
      }

  }

  ionViewDidLoad() {
    this._LOCAL_DATABASE.getUserData();
    this.getSearchHistory();
  }

  getSearchHistory(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._LOCAL_DATABASE.getSearchHistory()
        .then((result) => {
          this.searchHistory = result;
          console.log(result);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  }

  getUserData(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._LOCAL_DATABASE.getUserData()
        .then((result) => {
          this.uid = result.uid;
        })
        .catch(err => {
          console.log(err);
        });
    });
  }

  search(event): Promise<any> {
    this.offset = null;

    return new Promise((resolve, reject) => {
      if(event.trim() != '') {
        let pattern: string = event;
        this.searched = true;
        this.loading = true;
        this.alerts = [];
        this.users = [];
        this.searchbar.value = event;

        if (this.option == 'chapas') {
          let params: any = { uid: this.uid, chapa: pattern, offset: this.offset };
          params.chapa = params.chapa.toUpperCase();
          this.searchAlerts(params);
        }
        else {
          this.searchUsers(pattern)
          .then(()=>{
            resolve(true);
          })
          .catch(()=>{
            reject(true);            
          });
        }

      }
      else {
        resolve(true);
      }
    });
  }

  searchAlerts(params): Promise<any> {

    return new Promise((resolve, reject) =>{

      if(this.mode == 'searchAll') {

        this._DB.getAlerts(params)
        .then((result) => {
    
          if (result.length > 0) {
    
          this.alerts = this.alerts.concat(result);
          this.offset = result[result.length - 1].chapas[params.chapa];
          this.noResultsFound = false;
          }
          else {
          this.noResultsFound = true;
          }
    
          this.loading = false;
          let found = false;
    
          for (let i = 0; i < this.searchHistory.length; i++) {
    
          if (this.searchHistory[i].pattern.toLowerCase() == params.chapa.toLowerCase()) {
            found = true;
            break;
          }
          }
    
          if (!found) {
          let isTag: boolean = false;
    
          if(result.length > 0) {
            params.chapa = params.chapa.toUpperCase();
            isTag = true;
          }
    
          this._LOCAL_DATABASE.insertSearchPattern(params.chapa, isTag)
          .then((result) => {
            this.searchHistory.push({id: 0, pattern: params.chapa, isTag: isTag});
          })
          .catch((err) => {
            console.log(err);
          });
          }
    
          this.loading = false;
          resolve(true);
        })
        .catch((err) => {
          console.log(err);
          this.loading = false;
          reject(true);
        });

      }
      else if(this.mode == 'createdAlerts') {
        this.loading = false;
      }
      else if(this.mode == 'receivedAlerts') {

        /*this._DB.getReceivedAlerts(parameters)
        .then((data) => {
          if (data.length > 0) {
            this.alerts = this.alerts.concat(data);
            this.offset = data[data.length - 1].registDate;
          }

          if(this.alerts.length == 0) {
            this.noResultsFound = true;
          }
*/
          this.loading = false;
          /*
          resolve(true);
        });
*/
      }

    });
  }

  searchUsers(displayName: string): Promise<any> {

    this.loading = true;

    return new Promise((resolve, reject) => {

      this._DB.getUserData({displayName: displayName, offset: this.offset})
        .then((result) => {

          this.users = this.users.concat(result);
				  this.offset = result[result.length - 1].displayNameInsensitive;
          let found = false;

          for (let i = 0; i < this.searchHistory.length; i++) {
            if (this.searchHistory[i].pattern.toLowerCase() == this.searchbar.value.toLowerCase()) {
              found = true;
              break;
            }
          }

          if(this.users.length > 0) {
            this.noResultsFound = false;
          }
          else {
            this.noResultsFound = true;
          }

          if (!found) {

            this._LOCAL_DATABASE.insertSearchPattern(this.searchbar.value, false)
              .then((result) => {
                console.log('ok usuario guardado');
              })
              .catch((err) => {
                console.log(err);
              });
          }

				  this.loading = false;
        })
        .catch((err) => {
          this.loading = false;
          console.log(err);
        });
      });
  }

  elapsedTime(pDate): string {
    let date: any = new Date(pDate),
      now: any = new Date(), diff, hours, minutes, seconds, elapsedTime;

    diff = Math.floor(now - date);

    if (diff < 86400000) {
      hours = Math.floor(diff / 3600000);

      if (hours > 0) {
        elapsedTime = hours + " h";
      }
      else {

        minutes = Math.floor(diff / 60000);

        if (minutes > 0) {
          elapsedTime = minutes + " min";
        }
        else {
          elapsedTime = " un momento";
        }
      }

      elapsedTime = "hace " + elapsedTime;
    }
    else {
      let dateString = pDate.toString();
      //elapsedTime = this.formatDate(dateString.substr(0, dateString.indexOf('GMT') - 4));
      elapsedTime = dateString.substr(0, dateString.indexOf('GMT') - 4);
    }

    return elapsedTime;
  }

  openPage(page, params) {
    this.navCtrl.push(page, params);
  }

  presentPopover(ev, alert) {
    let options: any = [];
    if (alert.uid == this.uid) {
      options.push({ optionName: 'Eliminar', component: 'DeleteAlertPage', alert: alert });
    }
    else {
      options.push({ optionName: 'Denunciar', component: 'DenunciatePage', alertId: alert });
    }

    let popover = this.popoverCtrl.create('PopoverPage', {
      options: options
    });

    popover.present({
      ev: ev
    });

  }

  loadNextImage(alertId: string) {
    let y = this.alerts.find(x => x.id == alertId);
    let x = y.images.find(x => x.visible == 0);
    x.visible = 1;
  }

  deleteHistory(id): Promise<any>{
    return new Promise((resolve, reject) => {
      this._LOCAL_DATABASE.deleteSearchHistory(id)
        .then((result) => {
          this.getSearchHistory();
          this.presentToast('Se ha eliminado la búsqueda del historial');
          resolve(true);
        })
        .catch((err) => {
          reject(err);
        });
    })
  }

  confirmDeleteSearchHistory(id) {
    let alert = this.alertCtrl.create({
      title: 'Eliminar búsqueda',
      message: '¿Estás seguro que deseas eliminar esta búqueda de tu historial?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sí',
          handler: () => {
            this.deleteHistory(id);
          }
        }
      ]
    });
    alert.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  onSegmentChanged(event) {

    this.searchUsers(this.searchbar.value)
    .then(()=>{
	  console.log('termino la busqueda bien');
    })
    .catch(()=>{
	  console.log('termino la busqueda mal');
    });

  }

  refresh(refresher) {
    setTimeout(() => {
      this.offset = null;
      this.alerts = [];
      this.searchAlerts(this.searchbar.value);
      refresher.complete();
    }, 2000);
  }

  doInfinite(infiniteScroll) {

    this.loading = true;
    infiniteScroll.enable(false);

	if(this.option == 'chapas') {
		this.searchAlerts(this.searchbar.value)
		.then(result => {
			infiniteScroll.complete();

			if (this.firstAlert != this.alerts[this.alerts.length-1].id) {
			  this.firstAlert = this.alerts[this.alerts.length-1].id;
			  infiniteScroll.enable(true);
			}
		});
	}
	else if(this.option == 'users') {
		this.searchUsers(this.searchbar.value)
        .then(result => {
			infiniteScroll.complete();
			if(this.firstUser != this.users[this.users.length-1].uid) {
				this.firstUser = this.users[this.users.length-1].uid;
				infiniteScroll.enable(true);
			}
		})
		.catch((err)=>{
			infiniteScroll.complete();
			console.log(err);
		});
	}
  }
}
