import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private menuCtrl: MenuController) {
      this.menuCtrl.swipeEnable(false);
  }

  ionViewDidLoad() {

  }

}
