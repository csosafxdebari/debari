import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FollowTagsPage } from './follow-tags';

@NgModule({
  declarations: [
    FollowTagsPage,
  ],
  imports: [
    IonicPageModule.forChild(FollowTagsPage),
  ],
})
export class FollowTagsPageModule {}
