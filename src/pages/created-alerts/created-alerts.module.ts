import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreatedAlertsPage } from './created-alerts';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { DirectivesModule } from '../../directives/directives.module';
import { IonicImageLoader } from 'ionic-image-loader';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    CreatedAlertsPage
  ],
  imports: [
    IonicPageModule.forChild(CreatedAlertsPage),
    PipesModule,
    TruncateModule,
    IonicImageLoader,
    DirectivesModule
  ],
})
export class CreatedAlertsPageModule {}
