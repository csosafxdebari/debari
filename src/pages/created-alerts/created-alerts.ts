import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
  PopoverController,
  Events,
  ModalController} from 'ionic-angular';

import { PreloaderProvider } from './../../providers/preloader/preloader';
import { DatabaseProvider } from './../../providers/database/database';
import { AlertProvider } from './../../providers/alert/alert';
import { LocalDatabaseProvider } from './../../providers/local-database/local-database';
import { PhotoViewer } from '@ionic-native/photo-viewer';
//import { AutoHideDirective } from '../../directives/auto-hide/auto-hide';

@IonicPage()
@Component({
  selector: 'page-created-alerts',
  templateUrl: 'created-alerts.html',
})
export class CreatedAlertsPage {
  private alerts: any = []; // definir su tipo de dato
  public limit: string;
  public offset: string = null;
  private uid: string;
  public displayName: string;
  public firstAlert: string;
  public readLimit: number = 90;
  private noAlertsCreated: boolean = false; 
  //public truncating = true;
  
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public _LOADER: PreloaderProvider,
    public _DB: DatabaseProvider,
    private menu: MenuController,
    private _ALERT: AlertProvider,
    private _LOCALDB: LocalDatabaseProvider,
    private popoverCtrl: PopoverController,
    public events: Events,
    public modalCtrl: ModalController,
    private photoViewer: PhotoViewer) {

    events.subscribe('alert:deleted', (alertId) => {
      this.alerts.forEach((doc, index) => {
        if (doc.id == alertId) this.alerts.splice(index, 1);
      });
    });

    events.subscribe('alert:created', (alert: any = []) => {
      this.alerts = alert.concat(this.alerts);
    });

    events.subscribe('alert:updated', (alert: any = []) => {
      this.alerts.forEach((doc, index) => {
        console.log(alert);
        if (doc.id == alert.id) {
          this.alerts[index] = alert;
        }
      });
    });
  }

  ionViewDidLoad() {
    this._LOCALDB.getUserData()
      .then(result => {
        this.uid = result.uid;
        this.displayName = result.display_name;
        let email = result.email;
        let photoURL = result.photo_url;
        this.loadAndParseAlerts();
      })
      .catch((err) => {
        this._ALERT.presentAlert('Error', err.message);
      });
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(true);
  }

  loadAndParseAlerts(): Promise<any> {
    let params: any = { uid: this.uid, offset: this.offset };
    return new Promise((resolve) => {
      this._DB.getMyCreatedAlerts(params)
        .then((data) => {
          if (data.length > 0) {
            this.alerts = this.alerts.concat(data);
            this.offset = data[data.length - 1].registDate;
          }

          if(this.alerts.length == 0) {
            this.noAlertsCreated = true;
          }
          
          resolve(true);
        });
    });
  }

  openPage(page, params) {
    this.navCtrl.push(page, params);
  }

  doInfinite(infiniteScroll) {
    infiniteScroll.enable(false);

    this.loadAndParseAlerts()
      .then(result => {
        infiniteScroll.complete();
        if (this.firstAlert != this.alerts[0].id) {
          this.firstAlert = this.alerts[0].id;
          infiniteScroll.enable(true);
        }
      });
  }

  refresh(refresher) {
    setTimeout(() => {
      this.offset = null;
      this.alerts = [];
      this.loadAndParseAlerts();
      refresher.complete();
    }, 2000);
  }

  elapsedTime(pDate): string {
    let date: any = new Date(pDate),
      now: any = new Date(), diff, hours, minutes, seconds, elapsedTime;

    diff = Math.floor(now - date);

    if (diff < 86400000) {
      hours = Math.floor(diff / 3600000);

      if (hours > 0) {
        elapsedTime = hours + " h";
      }
      else
      {

        minutes = Math.floor(diff / 60000);

        if (minutes > 0) {
          elapsedTime = minutes + " min";
        }
        else
        {
          elapsedTime = " un momento";
        }
      }

      elapsedTime = "hace " + elapsedTime;
    }
    else
    {
      let dateString = pDate.toString();
      //elapsedTime = this.formatDate(dateString.substr(0, dateString.indexOf('GMT') - 4));
      elapsedTime = dateString.substr(0, dateString.indexOf('GMT') - 4);
    }
    
    return elapsedTime;
  }

  likeIt(alert) {
    let alertId = alert.id;
    let alertLiked = alert.userLiked || 0;
    let likesCount = alert.likesCount || 0;

    if (alert.userLiked == 0) {
      alert.userLiked = 1;
      alert.likesCount = likesCount + 1;
      let parameters = { id: alert.id + ":" + this.uid, userLiked: alert.userLiked };
      this._DB.likePost(parameters); 
    }
    else
    {
      alert.userLiked = 0;
      alert.likesCount = likesCount - 1;
      let parameters = { alertId: alert.id, userId: this.uid };
      this._DB.deleteDocument('usersLikes', alert.id + ":" + this.uid);
    };
  }

  // debe ir a otro provider

  formatDate(pDate): string {
    let day, dayName, dayNum, month, monthName, numMonth, year, hour, date;

    day = pDate.substring(0, 3);
    console.log("day" + day);
    switch (day) {
      case 'Sun':
        dayName = "Dom";
        break;
      case 'Mon':
        dayName = "Lun";
        break;
      case 'Tue':
        dayName = "Mar";
        break;
      case 'Wed':
        dayName = "Mié";
        break;
      case 'Thu':
        dayName = "Jue";
        break;
      case 'Fri':
        dayName = "Vie";
        break;
      case 'Sat':
        dayName = "Sáb";
        break;
    }

    month = pDate.substring(4, 7);

    switch (month) {
      case 'Jan':
        monthName = "ene";
        break;
      case 'Feb':
        monthName = "feb";
        break;
      case 'Mar':
        monthName = "mar";
        break;
      case 'Apr':
        monthName = "abr";
        break;
      case 'May':
        monthName = "may";
        break;
      case 'Jun':
        monthName = "jun";
        break;
      case 'Jul':
        monthName = "jul";
        break;
      case 'Aug':
        monthName = "ago";
        break;
      case 'Sep':
        monthName = "set";
        break;
      case 'Oct':
        monthName = "oct";
        break;
      case 'Nov':
        monthName = "nov";
        break;
      case 'Dic':
        monthName = "dic";
        break;
    }

    year = pDate.substring(11, 15);
    hour = pDate.substring(16, 21);

    date = dayName + " " + dayNum + " " + monthName + ". " + year + " " + hour;

    return date;
  }

  presentPopover(ev, alert) {
    let options: any = [];
    if (alert.uid == this.uid) {
      options.push({ optionName: 'Editar', component: 'ManageAlertPage', alert: alert });
      options.push({ optionName: 'Eliminar', component: 'DeleteAlertPage', alert: alert });
    }
    else {
      options.push({ optionName: 'Denunciar', component: 'DenunciatePage', alertId: alert });
    }

    let popover = this.popoverCtrl.create('PopoverPage', {
      options: options
    });

    popover.present({
      ev: ev
    });

  }
  
  loadNextImage(alertId: string) {
    let y = this.alerts.find(x => x.id == alertId);
    let x = y.images.find(x => x.visible == 0);
    x.visible=1;
  }

  currentUserPost(userId: string): boolean {
    if (userId == this.uid) {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  presentFullImageModal(images: any) {
    let fullImageModal = this.modalCtrl.create('FullImagePage', { images: images });
    fullImageModal.present();
  }

  presentModal(page, params) {
    let modal = this.modalCtrl.create(page, params, { cssClass: 'comments-modal' });
    modal.present();
  }

  viewPhoto(photo) {
    //if img
    this.photoViewer.show(photo, '', { share: true });
    //else
    //video
  }
}
