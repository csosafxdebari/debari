import { Component } from '@angular/core';
import {
  App,
  IonicPage,
  NavController,
  NavParams} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  tabs: any = [{ component: 'UserNewsFeedPage', icon: 'md-contacts'},
  {component: 'GlobalNewsFeedPage', icon: 'md-globe'},
  {component: 'NotificationsPage', icon: 'md-notifications'},
  {component: 'RoomsPage', icon: 'md-mail'}];

  constructor(public navCtrl: NavController,
    public navParams: NavParams, private app: App) {}

  openPage(page, params) {
    console.log(page);

    if(page == 'SearchPage'){
      this.navCtrl.push(page, params);
    }
  }
}
