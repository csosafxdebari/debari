import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import { IonicImageLoader } from 'ionic-image-loader';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ProfilePage,
    //AutoHideDirective
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    IonicImageLoader,
    TruncateModule,
    PipesModule
  ],
})
export class ProfilePageModule {}
