import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GlobalNewsFeedPage } from './global-news-feed';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { DirectivesModule } from '../../directives/directives.module';
import { IonicImageLoader } from 'ionic-image-loader';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    GlobalNewsFeedPage,
  ],
  imports: [
    IonicPageModule.forChild(GlobalNewsFeedPage),
    TruncateModule,
	  IonicImageLoader,
    DirectivesModule,
    PipesModule,
	  ComponentsModule
  ],
})
export class GlobalNewsFeedPageModule {}
