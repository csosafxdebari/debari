import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,
  MenuController,
  PopoverController,
  Events,
  ModalController, App } from 'ionic-angular';


import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';

import { PreloaderProvider } from './../../providers/preloader/preloader';
import { DatabaseProvider } from './../../providers/database/database';
import { AlertProvider } from './../../providers/alert/alert';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-global-news-feed',
  templateUrl: 'global-news-feed.html',
})
export class GlobalNewsFeedPage {
  private alerts: any = []; // definir su tipo de dato
  public limit: string;
  public offset: string = null;
  private uid: string;
  public displayName: string;
  public firstAlert: string;
  public readLimit: number = 90;
  private loading: boolean = false;
  private loadProgress: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _LOADER: PreloaderProvider,
    public _DB: DatabaseProvider,
    private menu: MenuController,
    private _ALERT: AlertProvider,
    private popoverCtrl: PopoverController,
    public events: Events,
    public modalCtrl: ModalController,
    private photoViewer: PhotoViewer,
    private streamingMedia: StreamingMedia,
    private _LOCAL_STORAGE: LocalStorageProvider,
    private app: App) {
      events.subscribe('alert:deleted', (alertId) => {
        this.alerts.forEach((doc, index) => {
          if (doc.id == alertId) this.alerts.splice(index, 1);
        });
      });
  
      events.subscribe('alert:created', (alert: any = []) => {
        
        this.alerts = alert.concat(this.alerts);
  
      });
  
      events.subscribe('alert:updated', (alert: any = []) => {
        this.alerts.forEach((doc, index) => {
  
          if (doc.id == alert.id) {
            this.alerts[index] = alert;
          }
        });
      });
  
      events.subscribe('alert:progress', (progress) => {
        this.loadProgress = progress;
  
        if(this.loadProgress == 100) {
          setTimeout(() => {
            
            
            this.loadProgress = 0;
          }, 3000);
        }
      });
  }

  ionViewDidLoad() {

    this._LOCAL_STORAGE.get('ls_user')
    .then((result)=>{

      this.uid = result.uid;
      this.displayName = result.displayName;
      let email = result.email;
      let photoURL = result.photoUrl;
      this.events.publish('user:loggedin', this.uid, this.displayName, email, photoURL);

      this.loadAndParseAlerts();
    })
    .catch((err)=>{
      this._ALERT.presentAlert('Error', err.message);
    });

    /* para quitar */

    this._DB.getFollowers()
    .then((result) => {
      console.log(result);

      result.forEach((doc)=>{
        console.log(doc.data());
      })
    })
    .catch((err) => {
      console.log(err);
    })
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(true);
  }

  
  loadAndParseAlerts() {
    this.loading = true;
    let params: any = { uid: this.uid, offset: this.offset };

    return new Promise((resolve, reject)=>{
      this._DB.getAlerts(params)
      .then((data) => {

        if (data.length > 0) {
          console.log(data);
          
          this.alerts = this.alerts.concat(data);
          this.offset = data[data.length - 1].registDate;
        }
        this.loading = false;
        return resolve(true);
      })
      .catch(err => {
        this.loading = false;
        return reject(true);
      });
    });


  }

  openPage(page, params) {
    this.app.getRootNav().push(page, params);
  }

  doInfinite(infiniteScroll) {
    infiniteScroll.enable(false);

    this.loadAndParseAlerts()
      .then(result => {
        infiniteScroll.complete();

        if (this.firstAlert != this.alerts[this.alerts.length - 1].id) {
          this.firstAlert = this.alerts[this.alerts.length - 1].id;
          infiniteScroll.enable(true);
        }
      });
  }

  refresh(refresher) {
    setTimeout(() => {
      this.offset = null;
      this.alerts = [];
      this.firstAlert = null;
      console.log(this.firstAlert);

      this.loadAndParseAlerts();
      refresher.complete();
    }, 2000);
  }

  likeIt(alert) {
    let alertId = alert.id;
    let alertLiked = alert.userLiked || 0;
    let likesCount = alert.likesCount || 0;

    if (alert.userLiked == 0) {
      alert.userLiked = 1;
      alert.likesCount = likesCount + 1;
      let parameters = { id: alert.id + ":" + this.uid, userLiked: alert.userLiked };
      this._DB.likePost(parameters); 
    }
    else
    {
      alert.userLiked = 0;
      alert.likesCount = likesCount - 1;
      let parameters = { alertId: alert.id, userId: this.uid };
      this._DB.deleteDocument('usersLikes', alert.id + ":" + this.uid);
    };
  }

  presentPopover(ev, alert) {
    let options: any = [];
    if (alert.uid == this.uid) {
     // options.push({ optionName: 'Editar', component: 'ManageAlertPage', alert: alert });
      options.push({ optionName: 'Eliminar', component: 'DeleteAlertPage', alert: alert });
    }
    else {
	  console.log(alert);
      options.push({ optionName: 'Denunciar', component: 'DenunciatePage', alertId: alert.id });
    }

    let popover = this.popoverCtrl.create('PopoverPage', {
      options: options
    });

    popover.present({
      ev: ev
    });

  }
  
  loadNextImage(alertId: string) {
    let y = this.alerts.find(x => x.id == alertId);
    let x = y.images.find(x => x.visible == 0);
    x.visible=1;
  }

  currentUserPost(userId: string): boolean {
    if (userId == this.uid) {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  presentFullImageModal(images: any) {
    let fullImageModal = this.modalCtrl.create('FullImagePage', { images: images });
    fullImageModal.present();
  }

  presentModal(page, params) {
    let modal = this.modalCtrl.create(page, params, { cssClass: 'comments-modal' });
    modal.present();
  }

  viewFile(file) {

    if (!file.thumbnail) {
      this.photoViewer.show(file.src, '', { share: true });
    }
    else {
      let options: StreamingVideoOptions = {
        successCallback: () => { console.log('Video played') },
        errorCallback: (e) => { console.log('Error streaming') },
        shouldAutoClose: true
      };
      
      this.streamingMedia.playVideo(file.src, options);
    }
  }

}
