import { Component } from '@angular/core';
import {
  IonicPage, NavController, NavParams,
  Loading,
  LoadingController,
  Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatabaseProvider } from '../../providers/database/database';
import { AlertProvider } from '../../providers/alert/alert';
import { AuthProvider } from '../../providers/auth/auth';
import { NetworkProvider } from '../../providers/network/network';

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {
  public resetForm: FormGroup;
  public loading: Loading;
  public connection: boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    private _DB: DatabaseProvider,
    public _ALERT: AlertProvider,
    public authData: AuthProvider,
    public loadingCtrl: LoadingController,
    public _NETWORK: NetworkProvider,
    private events: Events) {

    this.resetForm = formBuilder.group({
      email: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(50), Validators.pattern('[A-Za-z0-9_@.]*'), Validators.required])]
    });

  }

  sendResetEmail() {
    if (this._NETWORK.getConnectionType() != 'none') {
      let email: string = this.resetForm.value.email;

      if (this.isEmail(email)) {
        this.sendResetUserPasswordEmail(email);
        this.navCtrl.push('VerifyEmailPage', { mode: "reset", email: email });
      } else {

        console.log('no es un email');
      }
    }
    else {
      this._ALERT.presentAlert('Error', 'No hay conexión a internet');
    }
  }

  sendResetUserPasswordEmail(email: string) {
    this.authData.resetPassword(email)
      .then(resetResult => {
        this.loading.dismiss();
      });
  }

  isEmail(usuario) {
    const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(usuario);

    if (re) {
      return true;
    }
    else {
      return false;
    }
  }

}
