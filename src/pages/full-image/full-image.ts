import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FullImagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-full-image',
  templateUrl: 'full-image.html',
})
export class FullImagePage {
  images: any = [];
  index: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.images = this.images.concat(this.navParams.get('images'));
    console.log(this.images);
    this.index = this.navParams.get('index');
  }

  ionViewDidLoad() {

  }

}
