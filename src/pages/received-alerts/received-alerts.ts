import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
  PopoverController,
  Events,
  ModalController
} from 'ionic-angular';

import { PreloaderProvider } from './../../providers/preloader/preloader';
import { DatabaseProvider } from './../../providers/database/database';
import { AlertProvider } from './../../providers/alert/alert';
import { LocalDatabaseProvider } from './../../providers/local-database/local-database';
import { AuthProvider } from './../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-received-alerts',
  templateUrl: 'received-alerts.html',
})
export class ReceivedAlertsPage {
  public alerts: any = []; // definir su tipo de dato
  public limit: string;
  public offset: string = null;
  public userId: string;
  public chapas: any = [];
  public readLimit: number = 90;
  private loading: boolean = true;
  private noAlertsReceived: boolean = false;
  //public truncating = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public _LOADER: PreloaderProvider,
    public _DB: DatabaseProvider,
    private menu: MenuController,
    private _ALERT: AlertProvider,
    private _LOCALDB: LocalDatabaseProvider,
    private popoverCtrl: PopoverController,
    public events: Events,
    public modalCtrl: ModalController,
    public auth: AuthProvider) {

    events.subscribe('alert:deleted', (alertId) => {
      this.alerts.forEach((doc, index) => {
        if (doc.id == alertId) this.alerts.splice(index, 1);
      });
    });

    events.subscribe('alert:created', (alert: any = []) => {
      this.alerts = alert.concat(this.alerts);
    });
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(true);
  }

  ionViewDidLoad() {
    this.loadAndParseAlerts();
  }

  loadAndParseAlerts(): Promise<any> {
    let parameters: any = { offset: this.offset, chapas: this.chapas };

    return new Promise((resolve) => {

      this._DB.getReceivedAlerts(parameters)
        .then((data) => {
          if (data.length > 0) {
		      	this.events.publish('alert:resetAlertsUnseen', { viewed: true });
            this.alerts = this.alerts.concat(data);
            this.offset = data[data.length - 1].registDate;
          }

          if(this.alerts.length == 0) {
            this.noAlertsReceived = true;
          }

          this.loading = false;
          resolve(true);
        });
    });

  }

  openPage(page, params) {
    this.navCtrl.push(page, params);
  }

  doInfinite(infiniteScroll) {
    infiniteScroll.enable(false);

    this.loadAndParseAlerts()
      .then(result => {
        infiniteScroll.complete();
        infiniteScroll.enable(true);
      });
  }

  refresh(refresher) {
    setTimeout(() => {
      this.offset = null;
      this.alerts = [];
      this.loadAndParseAlerts();
      refresher.complete();
    }, 2000);
  }

  likeIt(alert) {
    let alertId = alert.id;
    let alertLiked = alert.userLiked || 0;
    let likes = alert.likes || 0;

    if (alert.userLiked == 0) {
      alert.userLiked = 1;
      alert.likes = likes + 1;
      let parameters = { id: alert.id + ":" + this.userId, userLiked: alert.userLiked };
      this._DB.likePost(parameters);
    }
    else {
      alert.userLiked = 0;
      alert.likes = likes - 1;
      let parameters = { alertId: alert.id, userId: this.userId };
      this._DB.deleteDocument('usersLikes', alert.id + ":" + this.userId);
    };


  }

  // debe ir a otro provider

  formatDate(pDate): string {
    let day, month, numMonth, year, hour, date;

    year = pDate.slice(0, 4);
    numMonth = Number(pDate.slice(5, 7));
    switch (numMonth) {
      case 1:
        month = "ene";
        break;
      case 2:
        month = "feb";
        break;
      case 3:
        month = "mar";
        break;
      case 4:
        month = "abr";
        break;
      case 5:
        month = "may";
        break;
      case 6:
        month = "jun";
        break;
      case 7:
        month = "jul";
        break;
      case 8:
        month = "ago";
        break;
      case 9:
        month = "set";
        break;
      case 10:
        month = "oct";
        break;
      case 11:
        month = "nov";
        break;
      case 12:
        month = "dic";
        break;
    }

    day = pDate.slice(8, 10);
    hour = pDate.slice(11, 17);

    date = day + " " + month + ". " + year + " " + hour;

    return date;
  }

  presentPopover(ev, alert) {
    let options: any = [];

    if (alert.userId == this.userId) {
      options.push({ optionName: 'Editar', component: 'ManageAlertPage', alert: alert });
      options.push({ optionName: 'Eliminar', component: 'DeleteAlertPage', alert: alert });
    }
    else {
      options.push({ optionName: 'Denunciar', component: 'DenunciatePage', alertId: alert });
    }

    let popover = this.popoverCtrl.create('PopoverPage', {
      options: options
    });

    popover.present({
      ev: ev
    });

  }

  loadNextImage(alertId: string) {
    let y = this.alerts.find(x => x.id == alertId);
    let x = y.images.find(x => x.visible == 0);
    x.visible = 1;
  }

  currentUserPost(userId: string): boolean {
    if (userId == this.userId) {
      return true;
    }
    else {
      return false;
    }
  }

  presentFullImageModal(images: any) {
    let profileModal = this.modalCtrl.create('FullImagePage', { images: images });
    profileModal.present();
  }
}
