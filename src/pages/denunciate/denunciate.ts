import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-denunciate',
  templateUrl: 'denunciate.html',
})
export class DenunciatePage {
  private alertId: string;
  private otherDescription: string = '';
  private checkedIdx=0;

  private options = [
    {description: 'Es inapropiado'},
    {description: 'Es ofensivo'},
    {description: 'Es falso'},
    {description: 'Otro motivo'}
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, public _DB: DatabaseProvider) {
    this.alertId = this.navParams.get('alertId');
  }

  ionViewDidLoad() {

  }

  reportAlert() {    
    let params: any = {
      alertId: this.alertId,
      reportType: this.checkedIdx
    }

    if(this.checkedIdx == 3) {
      params["otherDescription"] = this.otherDescription;
    }

    this._DB.reportAlert(params)
      .then(result => {
        console.log('ok');
        // mostrar mensaje de que sera procesada y verificada la denuncia
        this.navCtrl.pop();
      })
      .catch(err => {
        console.log(err.message);
      });
  }
}