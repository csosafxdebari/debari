import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoomsPage } from './rooms';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    RoomsPage,
  ],
  imports: [
    IonicPageModule.forChild(RoomsPage),
    IonicImageLoader
  ],
})
export class RoomsPageModule {}
