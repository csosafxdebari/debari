import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, PopoverController, ViewController, Events } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { AwaitProvider } from '../../providers/await/await';
import { AuthProvider } from '../../providers/auth/auth';
import { LocalDatabaseProvider } from '../../providers/local-database/local-database';

import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-rooms',
  templateUrl: 'rooms.html',
})
export class RoomsPage {
  private rooms: any = [];
  private rooms$: any = [];

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private _DB: DatabaseProvider, 
    private _AWAIT: AwaitProvider,
    private _AUTH: AuthProvider,
    private _LOCAL_DATABASE: LocalDatabaseProvider,
    private app: App,
    private popoverCtrl: PopoverController,
    private events: Events) {
      events.subscribe('room:deleted', (roomId) => {
        this.rooms.forEach((doc, index) => {
          if (doc.id == roomId) this.rooms.splice(index, 1);
        });
      });

      events.subscribe('room:updated', (roomId, message) => {
        this.rooms.forEach((doc, index) => {
          if (doc.id == roomId) this.rooms[index].message = message;
        });
      });

      events.subscribe('room:messagesViewed', (roomId) => {
        this.rooms.forEach((doc, index) => {
          if (doc.id == roomId) this.rooms[index].unseenMsgs = 0;
        });
      });
    }

  ionViewDidLoad() {
    this.getChatRooms();
    this.getChatRoomsUnsaved();
  }

  async getChatRooms() {
    let err, result;

    [err, result] = await this._AWAIT.to(this._LOCAL_DATABASE.getChatRooms());
    if(err) console.log(err);

    this.rooms = result;
    console.log(this.rooms);
  }

  async getChatRoomsUnsaved() {
    let err, result;
    
    this._DB.getChatRooms()
    .onSnapshot((snapshot)=> {

      snapshot.docs.forEach(doc => {
  
        let users = Object.keys(doc.data().users).filter(filt => filt != this._AUTH.getUid());
        let roomObj = { id: doc.id, buddyId: users[0], userData: this._DB.getUserData({uid: users[0]}), message: "", date: "" };

        let message = this._DB.getUnreadMessages(doc.id).onSnapshot(msgResult => {
          if(msgResult.docs.length) {
            let message = msgResult.docs[0].data().message;
            let date = msgResult.docs[0].data().date;
            let senderId = msgResult.docs[0].data().senderId;
            this.addMessage(doc.id, {msgId: msgResult.docs[0].id, message: message, date: date, senderId: senderId} );
          }
        });

        roomObj["messageSnapshot"] = message;
        roomObj["unseenMsgs"] = 0;

        this.rooms$.push(roomObj);
      });

    });
  }

  openPage(page, params) {
    this.app.getRootNav().push(page, params);
  }

  presentPopover(ev, room) {
    let options: any = [];

    options.push({ optionName: 'Eliminar', component: 'DeleteRoom', room: room });

    let popover = this.popoverCtrl.create('RoomsPopoverPage', {
      options: options
    });

    popover.present({
      ev: ev
    });

  }

  async addMessage(roomId, data) {
    let idx = this.rooms$.findIndex(x => x.id == roomId);
    let err, result;

    if(idx >= 0) { // no va a funcionar cuando se puedan eliminar los chat rooms
      this.rooms$[idx]["msgId"] = data.msgId;
      this.rooms$[idx]["message"] = data.message;
      let date = new Date();
      this.rooms$[idx]["date"] = date;
      this.rooms$[idx]["senderId"] = data.senderId;

      [err, result] =  await this._AWAIT.to(this._LOCAL_DATABASE.insertChatRoom(this.rooms$[idx]));
      if(err) {
        [err, result] =  await this._AWAIT.to(this._LOCAL_DATABASE.updateChatRoom(this.rooms$[idx]));
        if(err) console.log(err)
      }

      [err, result] = await this._AWAIT.to(this._LOCAL_DATABASE.insertPrivateMessage(roomId, data));
      if(err) return Promise.reject(err);

      [err, result] = await this._AWAIT.to(this._DB.markRoomAsSaved(this.rooms$[idx].id, this.rooms$[idx]["msgId"]));
      if(err) return Promise.reject(err);

      let roomIdx = this.rooms.findIndex(x => x.id == roomId);
      
      if(roomIdx >= 0) {
        this.rooms[roomIdx]["message"] = this.rooms$[idx]["message"];
        this.rooms[roomIdx]["date"] = this.rooms$[idx]["date"];
        this.rooms[roomIdx]["unseenMsgs"] = 1;

        console.log(this.rooms[roomIdx]["unseenMsgs"]);

        this.rooms.sort((a,b) => {
          let c = new Date(a.date);
          let d = new Date(b.date);

          return c>d ? -1 : c<d ? 1 : 0;
        });

      }
      else {
        let userData = await this._AWAIT.to(this.rooms$[idx].userData);        
        this.rooms$[idx]["buddyName"] = this.rooms$[idx].userData["__zone_symbol__value"].displayName;
        this.rooms$[idx]["date"] = this.rooms$[idx]["date"];
        this.rooms$[idx]["unseenMsgs"] = 1;
        //console.log(this.rooms[idx]["unseenMsgs"]);
        this.rooms.unshift(this.rooms$[idx]);
      }

      return Promise.resolve(true);
    }
  }

  pipeDate(pDate) {
    moment.locale('es');

    if(typeof pDate === "string") {
      pDate = new Date(parseInt(pDate));
    }
    
    const currentDate = moment().format('DD/MM/YY');
    const date = moment(pDate).format('DD/MM/YY');
    
    if(currentDate == date) {
      const hour = moment(pDate).format("HH:mm");
      return hour;
    }
    else {
      return date;
    }
  }
}
