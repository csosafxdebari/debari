import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  Events,
  ViewController } from 'ionic-angular';

import { AlertProvider } from '../../providers/alert/alert';
import { AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-comments-popover',
  templateUrl: 'comments-popover.html',
})
export class CommentsPopoverPage {
  public options: any = [];

  constructor(
    public viewCtrl: ViewController,
    private navParams: NavParams,
    private navCtrl: NavController,
    private _ALERT: AlertProvider,
    private alertCtrl: AlertController,
    private _DB: DatabaseProvider,
    public toastCtrl: ToastController,
    public events: Events
  ) {

  }

  ngOnInit() {
    if (this.navParams.data) {
      this.options = this.navParams.data.options;
    }
  }

  makeOption(option) {
    if (option.optionName != "Eliminar") {
      this.navCtrl.push(option.component, { comment: option.comment });
    }
    else {
      this.deleteComment(option.comment.id);
    }

    this.close();
  }

  deleteComment(commentId) {
    let alert = this.alertCtrl.create({
      title: 'Confirmación',
      message: '¿Está seguro que desea eliminar este comentario?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sí',
          handler: () => {
            this._DB.deleteDocument('/alertComments', commentId)
              .then(result => {
                this.presentToast('Comentario eliminado');
                this.events.publish('comment:deleted', commentId);
                this.navCtrl.pop();
              })
              .catch(err => {
                this._ALERT.presentAlert('Error', err.message);
              });
          }
        }
      ]
    });
    alert.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
