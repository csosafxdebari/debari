import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewAlertPage } from './view-alert';
import { IonicImageLoader } from 'ionic-image-loader';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ViewAlertPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewAlertPage),
    IonicImageLoader,
    PipesModule
  ],
})
export class ViewAlertPageModule {}
