import { Injectable } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';

@Injectable()
export class PreloaderProvider {
  private loading: Loading;

  constructor(public loadingCtrl: LoadingController) { }

  displayPreloader(): void {
    this.loading = this.loadingCtrl.create({
      content: 'Favor, aguarde...'
    });

    this.loading.present();
  }

  hidePreloader(): void {
    this.loading.dismiss();
  }
}
