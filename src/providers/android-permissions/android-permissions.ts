import { Injectable } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { AwaitProvider } from './../await/await';

@Injectable()
export class AndroidPermissionsProvider {

  constructor(private androidPermissions: AndroidPermissions, private _AWAIT: AwaitProvider) { }

  async checkPermission(permission: string) {
    let err, res;

    [err, res] = await this._AWAIT.to(this.androidPermissions.checkPermission(permission));
    if(err) return err;

    return res;
  }

}
