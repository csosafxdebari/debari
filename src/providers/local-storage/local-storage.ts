import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AwaitProvider } from './../await/await';

@Injectable()
export class LocalStorageProvider {

  constructor(private storage: Storage, private _AWAIT: AwaitProvider) { }

  async ready(): Promise<any> {
    let err, result;

    [err, result] = await this._AWAIT.to(this.storage.ready());
    if (err) return Promise.reject(err);

    return Promise.resolve(result);
  }

  async set(key: string, value: any): Promise<any> {
    let err, result;

    [err, result] = await this._AWAIT.to(this.storage.set(key, value));
    if (err) return Promise.reject(err);

    return Promise.resolve(result);
  }

  async get(key: string): Promise<any> {
    let err, result;

    [err, result] = await this._AWAIT.to(this.storage.get(key));
    if (err) return Promise.reject(err);

    return Promise.resolve(result);
  }

  async remove(key: string): Promise<any> {
    let err, result;

    [err, result] = await this._AWAIT.to(this.storage.remove(key));
    if (err) return Promise.reject(err);

    return Promise.resolve(result);
  }
}