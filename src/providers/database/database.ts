import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';

import * as firebase from 'firebase';
import 'firebase/firestore';

import { VideoEditor } from '@ionic-native/video-editor';
import { StorageProvider } from './../storage/storage';

import moment from 'moment';
import { LocalDatabaseProvider } from './../local-database/local-database';

import { AwaitProvider } from './../await/await';
import { CreatedAlertsPage } from './../../pages/created-alerts/created-alerts';
import { GeocodingProvider } from './../geocoding/geocoding';
import { LocalStorageProvider } from '../local-storage/local-storage';

//import { map, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { of } from 'rxjs/observable/of';
import { switchMap, map } from 'rxjs/operators';

@Injectable()
export class DatabaseProvider {
  private _DB: any;
  user: any;

  constructor(public _STORAGE: StorageProvider,
    public events: Events,
    private videoEditor: VideoEditor,
    private _LOCALDB: LocalDatabaseProvider,
    private _AWAIT: AwaitProvider,
    private geocoding: GeocodingProvider,
    private _LOCAL_STORAGE: LocalStorageProvider) {

      this._DB = firebase.firestore();

  }
/*
  getChat(chatId) {
    return this._DB
      .collection('chats')
      .doc(chatId)
      .snapshotChanges()
      .pipe(
        map((doc:any) => {
          return { id: doc.payload.id, ...doc.payload.data() };
        })
      );
  }
*/

async sendPrivateMessage(msgObj, roomId = "", buddyId) {
  msgObj["date"] = firebase.firestore.FieldValue.serverTimestamp();
  msgObj["saved"] = {}; // cambiar luego (?)
  let d = new Date(0);
  msgObj["saved"][`${buddyId}`] = d;
  
  const docRef = await this._DB.collection('chats').doc(roomId).collection("messages").add(msgObj);

  return docRef.id;
}

async createChatRoom(buddyId) {
    const uid = firebase.auth().currentUser.uid;

    const users = {};
    users[uid] = firebase.firestore.FieldValue.serverTimestamp();
    users[buddyId] = firebase.firestore.FieldValue.serverTimestamp();
  
    const data = {
      users
    };

    const [err, docRef] = await this._AWAIT.to(this._DB.collection('chats').add(data));
    if(err) return Promise.reject(err);

    return Promise.resolve(docRef.id);
  }

  async sendMessage(chatId, content) {
    const uid = firebase.auth().currentUser.uid;

    const data = {
      uid,
      content,
      createdAt: firebase.firestore.FieldValue.serverTimestamp()
    };

    if (uid) {
      const ref = this._DB.collection('chats').doc(chatId);
      return ref.update({
        messages: this._DB.FieldValue.arrayUnion(data)
      });
    }
  }

  joinUsers(chat$: Observable<any>) {
    let chat;
    const joinKeys = {};
  
    return chat$.pipe(
      switchMap(c => {
        // Unique User IDs
        console.log(c);
        chat = c;
        const uids = Array.from(new Set(c.messages.map(v => v.uid)));
  
        console.log(uids);
        
        // Firestore User Doc Reads
        const userDocs = uids.map(u =>
          this._DB.doc(`users/${u}`).valueChanges()
        );
  
        return userDocs.length ? combineLatest(userDocs) : of([]);
      }),
      map((arr: any) => {
        arr.forEach(v => (joinKeys[(<any>v).uid] = v));
        chat.messages = chat.messages.map(v => {
          return { ...v, user: joinKeys[v.uid] };
        });
  
        return chat;
      })
    );
  }

  getChatRooms() {
    let user = firebase.auth().currentUser;
    let d = new Date(0);
    return this._DB.collection('chats').where("users." + user.uid, '>', d);
  }

  getUnreadMessages(docId, justPreview = false) {
    let user = firebase.auth().currentUser;
    let d = new Date(1970);

    if(justPreview) {
      return this._DB.collection("chats").doc(docId).collection("messages").where("saved", "==", false).orderBy("date").limit(1);
    }
    else {
      return this._DB.collection("chats").doc(docId).collection("messages").where(`saved.${user.uid}`, "<", d); //.orderBy('date');
    }
  }

  markRoomAsSaved(docId, msgId) {
    let user = firebase.auth().currentUser;
    let saved = {};
    saved[`saved.${user.uid}`] = firebase.firestore.FieldValue.serverTimestamp();
    return this._DB.collection("chats").doc(docId).collection("messages").doc(msgId).update(saved);
  }

  updateAlertFase1(alertId, alertData, imgSlides): Promise<any> {

    return new Promise((resolve, reject)=>{
      let imgsToDelete: any = [];
      let imgsToUpload: any = [];
      let imgs: any = [];

      imgSlides.forEach((img)=> {
        if(img.delete == 1) {
          imgsToDelete.push(img);
        }
        else if(img.blob) {
          imgsToUpload.push(img);
        }
        else {
          imgs.push(img);
        }
      });

      if (imgsToDelete.length > 0) {

        this._STORAGE.deleteImages(imgsToDelete)
          .then(result => {
            // temporal
            console.log('ok imagenes eliminadas');
          })
          .catch(err => {
            console.log(err);
          });
      }

      if (imgsToUpload.length > 0) {
        this._STORAGE.uploadImage(`alertImgs/`, imgsToUpload)
          .then((snapshot: any) => {

            for (let i in snapshot) {
              let obj = {};
              obj = snapshot[i].metadata.downloadURLs[0];
              imgs.push({src: obj});
            }

            this.updateAlert(alertId, alertData, imgs);
          }).catch(err => {
            console.log(err);
          });
      }
      else {  
        this.updateAlert(alertId, alertData, imgs);
      }

      resolve(true);
    })
  }

  updateAlert(alertId, alertData: any, imgs: any[]): Promise<any> {

    return new Promise((resolve, reject) => {
		let alertForm = alertData.alertForm;
		
		let 
		  myAlertHour: string = alertForm.alertHour,
		  description: string = alertForm.description,
		  images: any = imgs,
      alertDate: string = alertForm.alertDate + " " + myAlertHour,
      location: any = alertData.location;
      
      let chapasNo: Array<any> = alertForm.chapasNo;
      let obj = {};

      chapasNo.forEach((doc) => {
        obj[doc] = firebase.firestore.FieldValue.serverTimestamp();
      });

		let alert: any = {
		  updateDate: firebase.firestore.FieldValue.serverTimestamp(),
		  alertDate: alertDate,
		  chapasNo: obj,
		  description: description,
      images: images,
      location: location
		};

		this.updateDocument("alerts", alertId,
			alert
		)
		  .then((data) => {
              alert["id"] = alertId;
              let user = firebase.auth().currentUser;
              alert["displayName"] = user.displayName;
              alert["userImg"] = user.photoURL;

              let imgArr: any = [];

              alert.images.forEach(img => {
                let type: string = this.getFileType(img.src);
                imgArr.push({ src: img.src, type: type, visible: 1 });
              });

              let chapas: any = [];
              chapas = Object.keys(obj);

              delete alert["images"];
              delete alert["chapasNo"];

              alert["images"] = imgArr;
              alert["chapasNo"] = chapas;
              alert["userLiked"] = 0;
              alert.registDate = alertData.registDate;

              this.events.publish('alert:updated', alert);
		  })
		  .catch((err) => {
			  reject(err);
		  });
		resolve(true);
	});
  }

	async saveAlert(alertData, imgSlides): Promise<any> {
		let progress: number = 1;
		this.events.publish('alert:progress', progress);
		let alertForm = alertData.alertForm;

		let err, snapshot, token, data, result, ls_alerts: any = [], ls_imgSlides: any = [];
  
    [err, result] = await this._AWAIT.to(this._LOCAL_STORAGE.get('alerts'));
    progress = 5;
    this.events.publish('alert:progress', progress);

    if(result) {
      ls_alerts = ls_alerts.concat(result);
    }

    ls_alerts.push(alertData);

    [err, result] = await this._AWAIT.to(this._LOCAL_STORAGE.set('alerts', ls_alerts));
    if (err) return Promise.reject(err);

    progress = 10;
    this.events.publish('alert:progress', progress);

		[err, snapshot] = await this._AWAIT.to(this._STORAGE.uploadImage(`alertImgs/`, alertData.imgSlides));
		if (err) return Promise.reject(err);

		progress = 30;
		this.events.publish('alert:progress', progress);

		alertData["names"] = [];
          
		for (let i in snapshot) {
			let obj = {};
			obj = snapshot[i].metadata.downloadURLs[0];
			let name = snapshot[i].metadata.name;
			alertData.images.push({ src: obj });
			alertData.names.push(name);
		}

		let chapasNo: Array<any> = alertForm.chapasNo;
		let obj = {};

		chapasNo.forEach((doc) => {
			obj[doc] = firebase.firestore.FieldValue.serverTimestamp();
		});

		let myAlertHour: string = alertForm.alertHour,
			description: string = alertForm.description,
			images: any = alertData.images,
			names: any = alertData.names,
			alertType: any = alertForm.alertType,
			alertCity: string = null,
			alertCountry: string = null,
			userLocation: string = null,
			alertDate: string,
			alertHour: string;

		/* hacer lo mismo cuando la alerta no tenga ubicacion */
            
		if(alertData["userLocation"]) {
		  userLocation = alertData["userLocation"];
		}

		if(alertData["alertCity"]) {
		  alertCity = alertData["alertCity"];
		  alertCountry = alertData["alertCountry"];
		}

		alertDate = alertForm.alertDate + " " + myAlertHour;
		progress = 50;
		this.events.publish('alert:progress', progress);

		[err, token] = await this._AWAIT.to(firebase.auth().currentUser.getIdToken(true))
		if (err) return Promise.reject(err);

		progress = 60;
		this.events.publish('alert:progress', progress);

		let alert: any = {
		  registDate: firebase.firestore.FieldValue.serverTimestamp(),
		  uid: firebase.auth().currentUser.uid,
		  userLocation: userLocation,
		  alertDate: alertDate,
		  alertCity: alertCity,
		  alertCountry: alertCountry,
		  chapasNo: obj,
		  description: description,
		  alertType: alertType,
		  images: images,
		  names: names,
		  token: token
		};

		if (alertData["location"] != null) {
		  alert["location"] = alertData["location"];
		}
	  
		[err, data] = await this._AWAIT.to(this.addDocument("alerts", alert));

		progress = 80;
		this.events.publish('alert:progress', progress);

		alert["id"] = data.id;

    [err, result] = await this._AWAIT.to(this._LOCAL_STORAGE.remove('alerts'));

    [err, result] = await this._AWAIT.to(this._LOCAL_STORAGE.get('ls_user'));
  
    if(err) return Promise.reject(err);
    this.user = result;

		alert["displayName"] = this.user.displayName;
		alert["userImg"] = this.user.photoURL;

		let imgArr: any = [];
		
		alert.images.forEach((img, index) => {
		  let type: string = this.getFileType(img.src);
		  imgArr.push({ src: img.src, type: type, visible: 1 });

		  if(imgSlides[index].mediaType == 1) {
			imgArr[index]["thumbnail"] = imgSlides[index]["thumbnail"];
		  }
		});

		progress = 85;
		this.events.publish('alert:progress', progress);

		let chapas: any = [];
		chapas = Object.keys(obj);

		delete alert["images"];
		delete alert["chapasNo"];
	  
		alert["images"] = imgArr;
		alert["chapasNo"] = chapas;
		alert["userLiked"] = 0;
		alert.registDate = new Date();

		switch(alertForm.alertType) {
		  case 0:
			alertType = "Notificación";
			break;
		  case "1":
			alertType = "Accidente";
			break;
		  case "2":
			alertType = "Infracción de ley de tránsito";
			break;
		  case "3":
			alertType = "Robo";
			break;
		  default:
			alertType = "Sin categoría";
			break;
		}

		alert.alertType = alertType;

		let arr: any = [];
		arr.push(alert);

		progress = 100;
		this.events.publish('alert:progress', progress);

		this.events.publish('alert:created', arr);

		return Promise.resolve(true);
	}
  
  getUserData(params: any = {}): Promise<any> {
      let limit: number = 10;
 
      return new Promise((resolve, reject) => {

          if (params.hasOwnProperty('uid')) {
              let docRef = this._DB.collection("users").doc(params.uid);

              docRef.get().then((doc) => {
                  if (doc.exists) {
                      let obj: any = {};
                      obj = doc.data();
 
                      resolve(obj);
                  } else {
                      resolve("No such document!");
                  }
              }).catch((err) => {
                  reject(err);
              });
          }
          else if (params.hasOwnProperty('displayName')) {
            let displayName: string = this.removeAccents(params.displayName);
            let obj: any = [];
 
            if (params.displayName.indexOf(" ")>0)
            {
              let query = this._DB.collection('users').where("displayNameInsensitive", "==", displayName).limit(limit);
 
              query
                .get()
                .then((querySnapshot) => {
                  querySnapshot
                    .forEach((doc: any) => {
                      obj.push({
                        uid: doc.id,
                        displayName: doc.data().displayName,
                        checked: doc.data().checked
                      });
                    });
                  resolve(obj);
                })
                .catch((err) => {
                  reject(err);
                });
            }
            else {
              let dbPromises = [];
              let queryFirstname = this._DB.collection('users').where("patterns." + displayName, ">", "").orderBy("patterns." + displayName).limit(limit);
              
			        if (params.offset) {
                queryFirstname = queryFirstname
                  .startAfter(params.offset);
              }

			        queryFirstname.get()
			        .then(values => {
 
                values.docs
                  .forEach((doc: any) => {
                        obj.push({
                          uid: doc.id,
                          displayName: doc.data().displayName,
                          thumbnail: doc.data().thumbnail,
                          checked: doc.data().checked,
                          creationTime: doc.data().creationTime,
						              displayNameInsensitive: doc.data().displayNameInsensitive
                        });
 
                      });
 
                resolve(obj);
              })
              .catch((err) => { console.log(err) });
            }
          }
          else {
            /*
             * ver luego
             */

            let query = this._DB.collection('users').orderBy("displayNameInsensitive");
            let obj: any = [];

            query
            .get()
            .then((querySnapshot) => {
              querySnapshot
                .forEach((doc: any) => {
                  obj.push({
                    uid: doc.id,
                    displayName: doc.data().displayName,
                    checked: doc.data().checked
                  });
                });
              resolve(obj);
            })
            .catch((err) => {
              reject(err);
            });
          }
    });
  }

  getAlerts(params: any = {}): Promise<any> {
    let limit: number = 10;
    let obj: any = [];

    return new Promise((resolve, reject) => {
      let query = this._DB.collection('alerts').limit(limit);

      if (params.chapa) {

        query = query.orderBy("chapasNo." + params.chapa, "desc");

        if (params.offset) {
          query = query
            .startAfter(params.offset);
            //.where <> denunciados
          }
          else {
            let d = new Date(0);
            query = query.where("chapasNo." + params.chapa, ">", d);
          }

      }
      else if (params.alertId) {
        query = query.doc(params.alertId);
      }
      else {
        query = query.orderBy("registDate", "desc");

        if (params.offset) {
          query = query
            .startAfter(params.offset);
            //.where <> denunciados
          }
      }
	  
      query
        .get()
        .then((querySnapshot) => {
          let dbPromises = [];

          querySnapshot
            .forEach((doc: any) => {

              let imgs: any = [];
              let userLiked = this.userLiked(params.uid, doc.id);
              dbPromises.push(userLiked);

              let userData = this.getUserData({ uid: doc.data().uid });
              dbPromises.push(userData);
              
              doc.data().images.forEach((img, index) => {
                let fileName = img.src.substring(img.src.lastIndexOf('/') + 13, img.src.lastIndexOf('?'));
                let fileType = this.getURLFileType(fileName);

                imgs.push({ src: img.src, type: fileType, visible: 0 });

                if (img.hasOwnProperty('thumbnail')) { 
                  imgs[index]["thumbnail"] = img.thumbnail;
                }

                imgs[0].visible = 1;
              });

                userLiked
                  .then(result => {

                    userData
                      .then(data => {

                        let alertType;

                        switch(doc.data().alertType) {
                          case 0:
                            alertType = "Notificación";
                            break;
                          case "1":
                            alertType = "Accidente";
                            break;
                          case "2":
                            alertType = "Infracción de ley de tránsito";
                            break;
                          case "3":
                            alertType = "Robo";
                            break;
                          case "4":
                            alertType = "Publicidad";
                            break;
                          default:
                            alertType = "Sin categoría";
                            break;
                        }

                        obj.push({
                          id: doc.id,
                          uid: doc.data().uid,
                          displayName: data.displayName,
                          userChecked: data.checked || false,
                          userImg: data.thumbnail || 'assets/imgs/default-profile.jpg',
                          registDate: doc.data().registDate,
                          alertDate: doc.data().alertDate,
                          alertCity: doc.data().alertCity,
                          alertCountry: doc.data().alertCountry,
                          chapasNo: Object.keys(doc.data().chapasNo),
                          chapas: doc.data().chapasNo,
                          description: doc.data().description,
                          alertType: alertType,
                          images: imgs,
                          location: doc.data().location,
                          likesCount: doc.data().likesCount,
                          commentsCount: doc.data().commentsCount,
                          userLiked: result,
                          type: doc.data().type,

                          // "provisional"
                          truncating: true
                        });

                      });
                  })
                  .catch(err => {
                    reject(err);
                  });

                // ----------------------------
            });

          Promise.all(dbPromises).then(values => {
            resolve(obj);
          });
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  getAlert(alertId: string, uid: string): Promise<any> {
	  return new Promise((resolve, reject) => {
	    let docRef = this._DB.collection("alerts").doc(alertId);
	    let dbPromises: any = [];

	    docRef.get().then((doc) => {
		    if (doc.exists) {
			    let obj: any = {};
			    let imgs: any = [];
          let userLiked = this.userLiked(uid, doc.id);
          dbPromises.push(userLiked);

          let userData = this.getUserData({ uid: doc.data().uid });
          dbPromises.push(userData);

          doc.data().images.forEach((img) => {
            let fileName = img.src.substring(img.src.lastIndexOf('/') + 13, img.src.lastIndexOf('?'));
            let fileType = this.getURLFileType(fileName);

            imgs.push({ src: img.src, type: fileType, visible: 0 });
            imgs[0].visible = 1;
          });

          userLiked
          .then(result => {

            userData
              .then(data => {
                obj = {
                  id: doc.id,
                  uid: doc.data().uid,
                  displayName: data.displayName,
                  userImg: data.thumbnail || 'assets/imgs/default-profile.jpg',
                  registDate: doc.data().registDate,
                  alertDate: doc.data().alertDate,
                  chapasNo: Object.keys(doc.data().chapasNo),
                  description: doc.data().description,
                  images: imgs,
                  location: doc.data().location,
                  likesCount: doc.data().likesCount,
                  commentsCount: doc.data().commentsCount,
                  userLiked: result,

                  // "provisional"
                  truncating: true
                };

                resolve(obj);
              });
            })
            .catch(err => {
              reject(err);
            });
		    } else {
			    resolve("No such document!");
		    }
	    }).catch((err) => {
		    reject(err);
	    }); 
	  });
  }

  getMyCreatedAlerts(params: any = {}): Promise<any> {
    let limit: number = 10;
    let obj: any = [];

    return new Promise((resolve, reject) => {
      let query = this._DB.collection('alerts').where("uid", "==", params.uid).limit(limit).orderBy("registDate", "desc");

      if (params.offset) {
        query = query
          .startAfter(params.offset);
        //.where <> denunciados
      }

      query
        .get()
        .then((querySnapshot) => {
          let dbPromises = [];

          querySnapshot
            .forEach((doc: any) => {
              let userLiked = this.userLiked(params.userId, doc.id);
              dbPromises.push(userLiked);

              let userData = this.getUserData({ uid: doc.data().uid });
              dbPromises.push(userData);

              let imgs: any = [];

              doc.data().images.forEach((img) => {

                let fileName = img.src.substring(img.src.lastIndexOf('/') + 13, img.src.lastIndexOf('?'));
                let fileType = this.getURLFileType(fileName);

                imgs.push({ src: img.src, type: fileType, visible: 0 });
                ///verificar que tenga primero una imagen
                imgs[0].visible = 1;
              });

              userLiked
                .then(result => {

                  userData
                    .then(data => {
                      obj.push({
                        id: doc.id,
                        uid: doc.data().uid,
                        displayName: data.displayName,
                        userImg: data.thumbnail || 'assets/imgs/default-profile.jpg',
                        registDate: doc.data().registDate,
                        alertDate: doc.data().alertDate,
                        chapasNo: Object.keys(doc.data().chapasNo),
                        description: doc.data().description,
                        images: imgs,
                        location: doc.data().location,
                        likesCount: doc.data().likesCount,
                        commentsCount: doc.data().commentsCount,
                        userLiked: result,

                        // "provisional"
                        truncating: true
                      });
                    });
                })
                .catch(err => {
                  reject(err);
                });
            });

          Promise.all(dbPromises).then(values => {
            resolve(obj);
          });

        })
        .catch((err) => {
          reject(err);
        });

    });
  }

  getReceivedAlerts(params: any = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      let limit: number = 10;
      let obj: any = [];
      let dbPromises: any = [];
      let dbUserPromises: any = [];
      let uid = firebase.auth().currentUser.uid;
      let chapas: any = [];
      let d = new Date(0);

      let query = this._DB.collection('timelines').where('uid', '==', uid);

      if(params.offset) {
        query = query.offset(params.offset);
      }

      let alertsQuery = this._DB.collection('alerts');

      query.get()
      .then((querySnapshot) =>{

        querySnapshot
        .forEach((doc)=>{
          let alertQuery = alertsQuery.doc(doc.data().alertId );
          dbPromises.push(alertQuery.get());
        });

        Promise.all(dbPromises).then(values =>{
          values.forEach((docP: any) => {
            //console.log(docP.exists); // preguntar si la alarma sigue existiendo

            if(docP.exists) {
              let chapasNo = Object.keys(docP.data().chapasNo);
              let userLiked = this.userLiked(uid, docP.id);
              dbUserPromises.push(userLiked);
        
              let userData = this.getUserData({ uid: docP.data().uid });
              let imgs: any = [];
              dbUserPromises.push(userData);

              docP.data().images.forEach((img) => {
                let fileName = img.src.substring(img.src.lastIndexOf('/') + 13, img.src.lastIndexOf('?'));
                let fileType = this.getURLFileType(fileName);

                imgs.push({ src: img.src, type: fileType, visible: 0 });
                ///verificar que tenga primero una imagen
                imgs[0].visible = 1;
              });

              userLiked
                    .then(result => {
                  
                userData.then((data)=>{
                  obj.push({
                  id: docP.id,
                  uid: docP.data().uid,
                  displayName: data.displayName,
                  userImg: data.thumbnail || 'assets/imgs/default-profile.jpg',
                  registDate: docP.data().registDate,
                  alertDate: docP.data().alertDate,
                  chapasNo: chapasNo,
                  description: docP.data().description,
                  images: imgs,
                  location: docP.data().location,
                  likesCount: docP.data().likesCount,
                  commentsCount: docP.data().commentsCount,
                  userLiked: result,
                  // "provisional"
                  truncating: true
            
                  });
                });

              });
            }
          });

          Promise.all(dbUserPromises).then((result)=>{
            resolve(obj);
          })
          
        })
        
      })
      .catch((err)=>{
        reject(err);
      })
    });
}

  addDocument(collectionObj: string, dataObj: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this._DB
        .collection(collectionObj)
        .add(dataObj)
        .then((obj: any) => {
          resolve(obj);
        })
        .catch((err: any) => {
          reject(err);
        });
    });
  }

  setDocument(collectionObj: string, document: string, dataObj: any, setOps = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      this._DB
        .collection(collectionObj)
        .doc(document)
        .set(dataObj, setOps)
        .then((obj: any) => {
          resolve(obj);
        })
        .catch((err: any) => {
          reject(err);
        });
    });
  }

  updateDocument(collectionObj: string,
    docID: string,
    dataObj: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this._DB
        .collection(collectionObj)
        .doc(docID)
        .update(dataObj)
        .then((obj: any) => {
          resolve("Document updated");
        })
        .catch((error: any) => {
          reject(error);
        });
    });
  }

  async deleteField(collectionObj: string,
    docID: string,
    fieldName: any) {
      let field: any = {};
      field[fieldName] = firebase.firestore.FieldValue.delete();
      let res = await this.updateDocument(collectionObj, docID, field);

      return res;

  }

  deleteDocument(collectionObj: string,
    docID: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this._DB
        .collection(collectionObj)
        .doc(docID)
        .delete()
        .then((obj: any) => {
          resolve(obj);
        })
        .catch((error: any) => {
          reject(error);
        });
    });
  }

  getTags(userId: string): Promise<any> {

    return new Promise((resolve, reject) => {

      let query = this._DB.collection('tags').doc(userId);

      query
        .get()
        .then((doc) => {

          let obj: any = {};
        if (doc.exists) {
            obj = Object.keys(doc.data());

            resolve(obj);
        } else {
            resolve(obj);
        }
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  async userLiked(userId: string, alertId: string) {

    try {

      let query = await this._DB.collection('usersLikes').doc(alertId + ":" + userId).get();
      let liked: number = query.exists ? 1 : 0;
      return liked;

    } catch (err) {
        return err;
    }

  }

  getAlertImgs(alertId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let obj: any = [];
      let query = this._DB.collection('alerts').doc(alertId)
        .get()
        .then((doc) => {

          if (doc.exists) {
            let imgTypes: any = [];

            doc.data().images.forEach((img) => {

              let fileName = img.src.substring(img.src.lastIndexOf('/') + 13, img.src.lastIndexOf('?'));
              let fileType = this.getURLFileType(fileName);

              imgTypes.push({ src: img.src, type: fileType });
            });

            obj.push({
              /*userImg: this._STORAGE.getUserImage(doc.data().userId),
              registDate: doc.data().registDate,
              alertDate: doc.data().alertDate,
              alertHour: doc.data().alertHour,
              chapaNo: doc.data().chapaNo,
              description: doc.data().description,*/
              images: imgTypes //,
              //imgsType: imgTypes
            });

            resolve(obj);
          }
          else {
            resolve(0);
          }

        })
        .catch(err => {
          reject(err);
        });
    });
  }

  likePost(parameters: any): Promise<any> {
    let currentDate: string = moment().format();

    return new Promise((resolve) => {

      if (parameters.userLiked == 1) {
        this._DB.collection('usersLikes').doc(parameters.id).set({ date: currentDate })
          .then(result => {
            console.log(result);

          });
      }
      else {
        this._DB.collection('usersLikes').doc(parameters.id).delete();
      }

      resolve(true);
    });
  }

  getURLFileType(file: string): string {
    let type = file.substr(file.lastIndexOf('.') + 1);

    return type;
  }

  getFileType(file: string): string {
    let fileName = file.substring(file.lastIndexOf('/') + 13, file.lastIndexOf('?'));
    let type = fileName.substr(fileName.lastIndexOf('.') + 1);

    return type;
  }

  getAlertComments(params: any = {}): Promise<any> {
    let limit: number = 10;
    let obj: any = [];
    let dbPromises = [];

    return new Promise((resolve, reject) => {
      let query = this._DB
        .collection('alertComments')
        .limit(limit)
        .orderBy("commentDate")
        .where("alertId", "==", params.alertId);

      if (params.offset) {
        console.log("params.offset: " + params.offset);
        query = query
          .startAfter(params.offset);
        //.where <> denunciados
      }

      query
        .get()
        .then((querySnapshot) => {

          querySnapshot
            .forEach((doc: any) => {

              let userData = this.getUserData({ uid: doc.data().uid });
              dbPromises.push(userData);

              userData
                .then(data => {
                  obj.push({
                    id: doc.id,
                    commentDate: doc.data().commentDate,
                    alertId: doc.data().alertId,
                    uid: doc.data().uid,
                    displayName: data.displayName,
                    userImg: data.thumbnail || 'assets/imgs/default-profile.jpg',
                    comment: doc.data().comment
                  });
                });
            });

          Promise.all(dbPromises).then(values => {
            resolve(obj);
          });

        })
        .catch(err => {
          console.log(err);
          reject(err);
        });
    });
  }

  getUserHistory(params: any = {}): Promise<any> {

    let limit: number = 10;
    let dbPromises = [];
    let dbUserPromises = [];
	  let obj: any = [];
	
    return new Promise((resolve, reject) => {
      let query = this._DB
        .collection('userHistory')
        .limit(limit);

      let dataQuery = query.where("uid", "==", params.uid).orderBy("created", "desc");
	    let alertRef = this._DB.collection("alerts");
	    let commentsRef = this._DB.collection("comments");

      if (params.offset) {
        dataQuery = dataQuery
          .startAfter(params.offset);
          //.where <> denunciados
        }

      dataQuery.get()
      .then((querySnapshot) => {

        let dbPromises = [];
        let icon;
        let description;

        querySnapshot
        .forEach((doc: any) => {	
			
          if(doc.data().type == 'alertSent') {

            let query = alertRef.doc(doc.data().documentId).get();
            dbPromises.push(query);
            
            query.then((result)=>{
              if (result.exists) { 
                let chapasNo = Object.keys(result.data().chapasNo);
                let chapaText: string = '';
                
                if(chapasNo.length == 1) {
                  chapaText = ' para la chapa ' + chapasNo;
                }
                else if(chapasNo.length > 1) {
                  chapaText = ' para las chapas ' + chapasNo;
                }
                
                //if(result.data().uid == params.uid) {
                  icon = 'assets/imgs/alert-send.png';

                if(result.data().uid == firebase.auth().currentUser.uid) {
                  description = 'Creaste una alerta ' + chapaText;
                }
                else {
                  description = 'Creó una alerta ' + chapaText;
                }
                  
                /*}
                else {
                  icon = 'assets/imgs/alert.png';

                  let userData = this.getUserData(result.data().uid);
                  dbPromises.push(userData);

                  description = 'Recibiste una alerta de ' + result.data().uid;
                }*/

                obj.push({
                  id: doc.id,
                  createdAt: doc.data().created,
                  documentId: doc.data().documentId,
                  type: doc.data().type,
                  icon: icon,
                  description: description
                });
              }
            });
          }
          else if(doc.data().type == 'alertReceived') {
            let query = alertRef.doc(doc.data().documentId).get();
            dbPromises.push(query);
            
            query.then((result) => {
              if (result.exists) { 
                  let userData = this.getUserData({uid: result.data().uid});
                  dbUserPromises.push(userData);

                  userData.then((data) =>{
                    let chapasNo = Object.keys(result.data().chapasNo);
                    let chapaText: string;
                    
                    if(chapasNo.length == 1) {
                      chapaText = ' para la chapa ' + chapasNo;
                    }
                    else if(chapasNo.length > 1) {
                      chapaText = ' para las chapas ' + chapasNo;
                    }

                    icon = 'assets/imgs/alert.png';

                    if(result.data().uid != firebase.auth().currentUser.uid) {
                      description = 'Recibiste una alerta de ' + data.displayName;
                    }
                    else {
                      description = 'Recibió una alerta de ' + data.displayName;
                    }

                    obj.push({
                      id: doc.id,
                      createdAt: doc.data().created,
                      documentId: doc.data().documentId,
                      type: doc.data().type,
                      icon: icon,
                      description: description
                    });
                  });
              }
            });
          }
          else if(doc.data().type == 'comment') {

            let userData = this.getUserData({uid: doc.data().commentUid});
            dbPromises.push(userData);

            userData.then((data)=>{
              icon = 'assets/imgs/comment.png';

              if(doc.data().uid != firebase.auth().currentUser.uid && data.uid != firebase.auth().currentUser.uid) {
                description = data.displayName + ' comentó su alerta.';
              }
              else if (doc.data().uid == firebase.auth().currentUser.uid) {
                description = data.displayName + ' comentó tu alerta.';
              }
            
              obj.push({
                id: doc.id,
                createdAt: doc.data().created,
                documentId: doc.data().documentId,
                alertId: doc.data().alertId,
                type: doc.data().type,
                icon: icon,
                description: description
              });
            })
            .catch((err)=>{
              console.log(err);
            });
          }
          else if (doc.data().type == 'userFollowing') {
            let userData = this.getUserData({uid: doc.data().followerId});
            dbPromises.push(userData);

            userData.then((data)=>{
              icon = 'assets/imgs/comment.png';

              if(doc.data().uid != firebase.auth().currentUser.uid && data.uid != firebase.auth().currentUser.uid) {
                description = data.displayName + ' comenzó a seguirle.';
              }
              else if (doc.data().uid == firebase.auth().currentUser.uid) {
                description = data.displayName + ' comenzó a seguirte.';
              }
            
              obj.push({
                id: doc.id,
                createdAt: doc.data().created,
                documentId: doc.data().documentId,
                type: doc.data().type,
                icon: icon,
                description: description
              });
            })
            .catch((err)=>{
              console.log(err);
            });
          }
        });
        
        Promise.all(dbPromises).then((result)=>{ 
          Promise.all(dbUserPromises).then((result)=>{
            resolve(obj);
          })
          .catch((errUser)=>{
            reject(errUser);
          })
        })
        .catch((err)=>{
          reject(err);
        });
        
      })
      .catch((err)=>{
        reject(err);
      });
    });
  }

  reportAlert(params: any): Promise<any> {

    return new Promise((resolve, reject) => {
      params.reportDate = firebase.firestore.FieldValue.serverTimestamp();
      params.reportState = 'A';
      let user = firebase.auth().currentUser;
      params["uid"] = user.uid;

      this.addDocument('alertDenunciation', params)
        .then(result => {
          resolve(true);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  async addComment(commentData) {
    let commentDate = firebase.firestore.FieldValue.serverTimestamp();
    let user = firebase.auth().currentUser;

    commentData["uid"] = user.uid;
    commentData["commentDate"] = commentDate;

    try
    {

      let result = await this.addDocument('/alertComments', commentData);
      commentData["id"] = result.id;
      commentData["userImg"] = user.photoURL;
      commentData["commentDate"] = moment().format();
      return commentData;

    } catch(err) {
          return err;
    };
  }

  async saveToken(token) {
    const devicesRef = this._DB.collection('devices');

    let user = await this._LOCALDB.getUserData();
    
    const docData = { 
      token,
      uid: user.uid
    }

    return devicesRef.doc(token).set(docData);

  }

  removeAccents(s) {
    let r = s.toLowerCase().trim();
    r = r.replace(new RegExp(/\s/g), "");
    r = r.replace(new RegExp(/[àáâãäå]/g), "a");
    r = r.replace(new RegExp(/[èéêë]/g), "e");
    r = r.replace(new RegExp(/[ìíîï]/g), "i");
    r = r.replace(new RegExp(/ñ/g), "n");
    r = r.replace(new RegExp(/[òóôõö]/g), "o");
    r = r.replace(new RegExp(/[ùúûü]/g), "u");

    return r;
  }

  async followUser(uid: string): Promise<any> {
    let user = firebase.auth().currentUser;
    let err, result;

    [err, result] = await this._AWAIT.to
      (this._DB
      .collection('/followers')
      .doc(user.uid)
      .collection('usersFollowing')
      .doc(uid)
      .set({followedAt: firebase.firestore.FieldValue.serverTimestamp()})
      );

    if(err) return Promise.reject(err);

    return Promise.resolve(true);
  }

  async unFollowUser(uid: string): Promise<any> {
    let user = firebase.auth().currentUser;
    let err, result;

    [err, result] = await this._AWAIT.to
    (this._DB
    .collection('/followers')
    .doc(user.uid)
    .collection('usersFollowing')
    .doc(uid)
    .delete()
    );

    if(err) return Promise.reject(err);

    return Promise.resolve(true);
  }

  async getUserFollowers(params: any = {}): Promise<any> {
    let user = firebase.auth().currentUser;
    let err, result;

    if (params.hasOwnProperty('uid')) {

      let docRef = this._DB.collection("/followers").doc(user.uid).collection('usersFollowing').doc(params.uid);

      [err, result] = await this._AWAIT.to(docRef.get());
      if (err) return Promise.reject(err);

      return Promise.resolve(result); 
    }
  }

  async getFollowers(): Promise<any> {
    const user = firebase.auth().currentUser;
    const followers = this._DB.collection('followers').doc(user.uid).collection('usersFollowing');

    let err, result;

    [err, result] = await this._AWAIT.to(followers.get());
    if(err) return Promise.reject(err);

    return Promise.resolve(result);

  }

  
  getUserNewsFeed(params: any = {}): Promise<any> {
    let limit: number = 10;
    let obj: any = [];

    return new Promise((resolve, reject) => {
      let user = firebase.auth().currentUser;
      let query = this._DB.collection('newsFeed').doc(user.uid).collection('alerts').limit(limit);

      if (params.chapa) {

        query = query.orderBy("chapasNo." + params.chapa, "desc");

        if (params.offset) {
          query = query
            .startAfter(params.offset);
            //.where <> denunciados
          }
          else {
            let d = new Date(0);
            query = query.where("chapasNo." + params.chapa, ">", d);
          }

      }
      else if (params.alertId) {
        query = query.doc(params.alertId);
      }
      else {
        query = query.orderBy("registDate", "desc");

        if (params.offset) {
          query = query
            .startAfter(params.offset);
            //.where <> denunciados
          }
      }
	  
      query
        .get()
        .then((querySnapshot) => {
          let dbPromises = [];

          querySnapshot
            .forEach((doc: any) => {

              let imgs: any = [];
              let userLiked = this.userLiked(params.uid, doc.id);
              dbPromises.push(userLiked);

              let userData = this.getUserData({ uid: doc.data().uid });
              dbPromises.push(userData);
              
              doc.data().images.forEach((img, index) => {
                let fileName = img.src.substring(img.src.lastIndexOf('/') + 13, img.src.lastIndexOf('?'));
                let fileType = this.getURLFileType(fileName);

                imgs.push({ src: img.src, type: fileType, visible: 0 });

                if (img.hasOwnProperty('thumbnail')) { 
                  imgs[index]["thumbnail"] = img.thumbnail;
                }

                imgs[0].visible = 1;
              });

                userLiked
                  .then(result => {

                    userData
                      .then(data => {

                        let alertType;

                        switch(doc.data().alertType) {
                          case 0:
                            alertType = "Notificación";
                            break;
                          case "1":
                            alertType = "Accidente";
                            break;
                          case "2":
                            alertType = "Infracción de ley de tránsito";
                            break;
                          case "3":
                            alertType = "Robo";
                            break;
                          case "4":
                            alertType = "Publicidad";
                            break;
                          default:
                            alertType = "Sin categoría";
                            break;
                        }

                        obj.push({
                          id: doc.id,
                          uid: doc.data().uid,
                          displayName: data.displayName,
                          userChecked: data.checked || false,
                          userImg: data.thumbnail || 'assets/imgs/default-profile.jpg',
                          registDate: doc.data().registDate,
                          alertDate: doc.data().alertDate,
                          alertCity: doc.data().alertCity,
                          alertCountry: doc.data().alertCountry,
                          chapasNo: Object.keys(doc.data().chapasNo),
                          chapas: doc.data().chapasNo,
                          description: doc.data().description,
                          alertType: alertType,
                          images: imgs,
                          location: doc.data().location,
                          likesCount: doc.data().likesCount,
                          commentsCount: doc.data().commentsCount,
                          userLiked: result,
                          type: doc.data().type,

                          // "provisional"
                          truncating: true
                        });

                      });
                  })
                  .catch(err => {
                    reject(err);
                  });

                // ----------------------------
            });

          Promise.all(dbPromises).then(values => {
            resolve(obj);
          });
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

}