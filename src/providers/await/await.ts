import { Injectable } from '@angular/core';

@Injectable()
export class AwaitProvider {

  constructor() { }

  to(promise) {
    return promise.then(data => {
       return [null, data];
    })
    .catch(err => [err]);
  }
}
