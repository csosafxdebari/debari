import { Injectable } from '@angular/core';
import {
  Events
} from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase/app';

import { DatabaseProvider } from '../database/database';
import { AlertProvider } from '../alert/alert';
import { LocalDatabaseProvider } from '../local-database/local-database';
import { StorageProvider } from '../storage/storage';
import { AwaitProvider } from './../await/await';
import { DiagnosticProvider } from './../diagnostic/diagnostic';
import { GeolocationProvider } from './../geolocation/geolocation';
import { GeocodingProvider } from '../geocoding/geocoding';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocalStorageProvider } from '../local-storage/local-storage';

@Injectable()
export class AuthProvider {
  session: any;

  constructor(public afAuth: AngularFireAuth,
	  public _DB: DatabaseProvider,
	  private _ALERT: AlertProvider,
	  private _LOCALDB: LocalDatabaseProvider,
    private _STORAGE: StorageProvider,
    private events: Events,
    private _AWAIT: AwaitProvider,
    private _DIAGNOSTIC: DiagnosticProvider,
    private _GEOLOCATION: GeolocationProvider,
    private _GEOCODING: GeocodingProvider,
    private androidPermissions: AndroidPermissions,
    private _LOCAL_STORAGE: LocalStorageProvider) {

  }

  signInWithFacebook(): Promise<any> {
    let provider = new firebase.auth.FacebookAuthProvider();

    return new Promise((resolve, reject) => {
      firebase.auth().signInWithRedirect(provider).then(() => {
        firebase.auth().getRedirectResult().then((result) => {
          resolve(result);
        })
        .catch(err => {
          reject(err);
        });
      });
    });
  }

  async loginUser(email: string, password: string): Promise<any> {
    console.log('loginUser');
    let err, result, diagnosticResult, geolocationResult, geocodingResult;

    [err, result] = await this._AWAIT.to(this.afAuth.auth.signInWithEmailAndPassword(email, password));
    if (err) return Promise.reject(err);

    let user = firebase.auth().currentUser;

    if (user.emailVerified) {

      [err, result] = await this._AWAIT.to(this._DB.getUserData({ uid: user.uid }));
      if (err) Promise.reject(err);
      
      let userLocation: any = {};

      if(result.userLocation) {
        userLocation = result.userLocation;
      }

      let displayName: string = result.displayName;

      if(Object.keys(userLocation).length === 0) {
        [err, result] = await this._AWAIT.to(this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION));

        if(result.hasPermission) {
          [err, diagnosticResult] = await this._AWAIT.to(this._DIAGNOSTIC.isGpsLocationEnabled());
          if(err) console.log(err);
  
          if(diagnosticResult) {
            [err, geolocationResult] = await this._AWAIT.to(this._GEOLOCATION.getCurrentPosition());
            if(err) console.log(err);
  
            if (geolocationResult) {
              userLocation = { latitude: geolocationResult.coords.latitude, longitude: geolocationResult.coords.longitude };
  
              this._DB.setDocument('/users', user.uid, {userLocation: userLocation}, {merge: true})
              .then(()=>{
                console.log('user location added');
              })
              .catch((err)=>{
                console.log(err);
              });
            }
          }
        }
      }

	  let userCountryCode: string = null;
	  
      if(Object.keys(userLocation).length != 0) {
        [err, result] = await this._AWAIT.to(this._GEOCODING.reverseGeocode(userLocation.latitude, userLocation.longitude));
        if(err) console.log(err);

        if(result) {
          userCountryCode = result[0].countryCode;
        }
      }

      let range: string = result.range;
      let points: number = result.points;
      let alertsCount: number = result.alertsCount;
      let photoURL: string = result.photoURL || 'assets/imgs/default-profile.jpg';
      let thumbnail: string = result.thumbnail || 'assets/imgs/thumb_default-profile.jpg';
      let creationTime: string = result.creationTime;

      this.userLoggedIn(user.uid, displayName, email, photoURL);
   
      let userObj = {
        uid: user.uid,
        displayName: displayName,
        email: user.email,
        photoURL: photoURL,
        thumbnail: thumbnail,
        creationTime: creationTime,
        userCountryCode: userCountryCode,
        range: range,
        points: points,
        alertsCount: alertsCount
      };

      [err, result] = await this._AWAIT.to(this._LOCALDB.saveUserProfile(userObj));
      if (err) Promise.reject(err);

      [err, result] = await this._AWAIT.to(this._LOCAL_STORAGE.ready());
      if (err) Promise.reject(err);

      [err, result] = await this._AWAIT.to(this._LOCAL_STORAGE.set('ls_user', userObj));
      if (err) Promise.reject(err);

      return Promise.resolve(true);

    } else {
      return Promise.reject({message: 'Por favor, verifica tu email para activar la cuenta'});
    }

  }

  resetPassword(email: string): Promise<void> {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  logoutUser(): Promise<void> {
    return this.afAuth.auth.signOut();
  }

  signupUser(email: string, password: string): Promise<any> {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  sendEmailVerification(): Promise<any> {

    return new Promise((resolve, reject) => {
      this.afAuth.authState.subscribe(user => {
        user.sendEmailVerification()
          .then(() => {
            resolve(true);
          })
          .catch((err) => {
            reject(true);
          })
      });
    });

  }

  async updateProfile(userData) {
    let user = firebase.auth().currentUser;
    try {
      user.updateProfile(userData);
    } catch (err) {
      return err;
    }
  }

  getUserProfile(): Promise<any> {
    return new Promise(resolve => {
      let user = firebase.auth().currentUser;
      let name, email, photoURL;

      if (user != null) {
        name = user.displayName;
        email = user.email;
        photoURL = user.photoURL;
      }
      resolve({ displayName: name, email: email, photoURL: photoURL });
    });
  }
  
   userLoggedIn(uid: string, usuario: string, email: string, photoURL: string) {
    this.events.publish('user:loggedin', uid, usuario, email, photoURL);
  }

  getUid() {
    return firebase.auth().currentUser.uid;
  }

  getDisplayName() {
    return firebase.auth().currentUser.displayName;
  }

  getPhotoURL() {
    return firebase.auth().currentUser.photoURL;
  }

  getEmail() {
    return firebase.auth().currentUser.email;
  }
}
