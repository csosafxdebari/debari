import { Injectable } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic';
import { AwaitProvider } from './../await/await';

@Injectable()
export class DiagnosticProvider {

  constructor(private diagnostic: Diagnostic, private _AWAIT: AwaitProvider) { }

  async isGpsLocationEnabled() {
    let err, res;

    [err, res] = await this._AWAIT.to(this.diagnostic.isGpsLocationEnabled());
    if(err) return err;

    return res;
  }


}
